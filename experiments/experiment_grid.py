import numpy as np
import pandas as pd

import config
from experiments import experiments


def toResultsRow(results):
    params = results.exp_params.return_parameters(ignore_parameters=['encoded_df_path'])
    row = [
        params['lr'],
        params['momentum'],
        params['bs'],
        params['enc_name'],
        "All" if not params['city'] else params['city'],
        params['pos_cls_sampling_mult'],
        ', '.join(params['drop_columns']),
        params['epochs'],
        params['preprocessing_name'],
        params['model_name'],
        results.loss[-1]]
    return row


def create_evaluation(result):

    print('train')
    print(result.train_loss)
    print(result.train_metrics)

    print('test')
    print(result.loss)
    print(result.metrics)

    evaluation_experiment = experiments.Experiment.build_evaluation(
        trained_model=result.model,
        data_preprocessor=result.preprocessor_data,
        target_preprocessor=result.preprocessor_target,
        **result.exp_params.return_parameters(
            ignore_parameters=['enc_name', 'pos_cls_sampling_mult', 'preprocessing_name'])
    )

    eval_results = evaluation_experiment.run_eval()

    print('eval')
    print(eval_results['eval'].losses)
    print(eval_results['eval'].metrics)


def to_dataframe(results):
    df = pd.DataFrame(data=[toResultsRow(x) for x in results.exps_results], columns=config.output_csv_columns)
    no_loss_columns = config.output_csv_columns.copy()
    no_loss_columns.remove('Loss')
    agg_df = df.groupby(no_loss_columns).agg({'Loss': [np.mean, np.std, len]})
    agg_df = agg_df.reset_index()
    agg_df.columns = no_loss_columns + config.aggregations_columns
    return agg_df


def filter_best_results(results):
    agg_df = to_dataframe(results)
    agg_df['Loss'] = agg_df['Loss min']
    agg_df.drop(config.aggregations_columns, axis=1, inplace=True)
    df = pd.DataFrame(data=[toResultsRow(x) for x in results.exps_results], columns=config.output_csv_columns)
    best_results = []
    for agg_index, agg_row in agg_df.iterrows():
        for index, row in df.iterrows():
            if agg_row.equals(row):
                best_results.append(results.exps_results[index])
                break
    return best_results
