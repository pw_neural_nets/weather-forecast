import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns


def get_preds(net, dataset, x):
    out = net.forward(x)
    out = out.detach().numpy()
    out = dataset.inv_preprocess_targets(out)
    out = out.reshape(-1).tolist()
    return out


def plot_preds(dataloader, model, title, save_path=None):
    preds = []
    targets = []
    dataset = dataloader.dataset
    for x, y in dataloader:

        out = get_preds(model, dataset, x)
        y = dataset.inv_preprocess_targets(y)

        preds += out
        targets += y.reshape(-1).tolist()
    preds = np.array(preds)
    targets = np.array(targets)

    plt.subplot(311)
    plt.title(title)
    plt.plot(preds, label='preds')
    plt.legend()

    plt.subplot(312)
    plt.plot(targets, label='targets')
    plt.ylabel('Temp ($^\circ$C)')
    plt.legend()

    plt.subplot(313)
    plt.plot(preds - targets, label='diff')
    plt.xlabel('Numer dnia')
    plt.legend()

    plt.tight_layout()

    if save_path:
        save_folder = save_path.parents[0]
        if not save_folder.exists():
            save_folder.mkdir(parents=True)

        plt.savefig(fname=save_path)
        plt.clf()
    else:
        plt.show()


def save_conf_matrix(rez, save_path):
    sns.set(font_scale=1.5)
    yticklabels = ['TP', 'FN']
    xticklabels = ['FN', 'TN']

    sns.heatmap([[rez['true_positive'], rez['false_positive']],
                 [rez['false_negative'], rez['true_negative']]],
                annot=True, fmt="d", cmap='coolwarm', yticklabels=yticklabels, xticklabels=xticklabels,
                cbar=False, annot_kws={"size": 15})

    plt.savefig(fname=save_path)
    plt.clf()


def save_plots(results, experiment, save_path, type_, is_baseline, exp_result):
    sns.set(font_scale=1)
    bl_prefix = 'bl_' if is_baseline else ''

    if type_ == 'cls':
        if is_baseline:
            train_rez = results['train'].metrics[-1]
            test_rez = results['test'].metrics[-1]
        else:
            train_rez = {k: int(abs(v.value)) for k, v in results.train_metrics[-1].items() if not np.isnan(v.value)}
            test_rez = {k: int(abs(v.value)) for k, v in results.metrics[-1].items() if not np.isnan(v.value)}

        save_conf_matrix(train_rez, save_path/f"conf_matrix/{bl_prefix}train-cm-{exp_result.exp_params.city}.png")
        save_conf_matrix(test_rez, save_path/f"conf_matrix/{bl_prefix}test-cm-{exp_result.exp_params.city}.png")

    elif type_ == 'reg':
        model = experiment.model
        dataloader = experiment.databunch['unshuffled_train']
        plot_preds(
            dataloader, model, title='',
            save_path=save_path/f"temp_plot/{bl_prefix}train-reg-{exp_result.exp_params.city}.png")

        dataloader = experiment.databunch['test']
        plot_preds(
            dataloader, model, title='',
            save_path=save_path/f"temp_plot/{bl_prefix}test-reg-{exp_result.exp_params.city}.png")

    else:
        print(f'different exp_type in plot {type}')


def save_training_plot(exp_result, target_img_path, model_to_metric_name):
    sns.set(font_scale=1)
    for split_type in [('train', 'train_metrics'), ('test', 'metrics')]:
        metrics_list = exp_result.__dict__[split_type[1]]

        metric_name = model_to_metric_name[exp_result.exp_params.model_name]
        metric_list = [m[metric_name] for m in metrics_list]

        mean_values = np.array([abs(x.value) for x in metric_list][1:])
        std = np.array([x.std for x in metric_list][1:])

        plt.plot(range(len(mean_values)), mean_values, label=split_type[0])
        plt.fill_between(range(len(mean_values)), y1=mean_values-std, y2=mean_values+std, alpha=.5)
        plt.xlabel('Epoch')
        plt.ylabel('Metric')
        plt.legend()

    image_path = (target_img_path/f"training/training-{exp_result.exp_params.city}"
                                  f"-{exp_result.exp_params.model_name}.png")

    if not image_path.parents[0].exists():
        image_path.parents[0].mkdir(parents=True)

    plt.savefig(fname=image_path)
    plt.clf()
