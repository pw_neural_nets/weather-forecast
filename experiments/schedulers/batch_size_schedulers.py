import torch


class BatchScheduler:
    def __init__(self, epochs_bs):
        self.epochs_bs = epochs_bs
        self.counter = 0

    def step(self, databunch):
        bs = self.epochs_bs[self.counter]
        self.set_bs(databunch, bs)
        self.counter += 1

    def set_bs(self, databunch, bs):
        data_loader = databunch.train

        new_data_loader = torch.utils.data.DataLoader(
            dataset=data_loader.__dict__['dataset'],
            batch_size=bs,
            num_workers=data_loader.__dict__['num_workers'],
            sampler=data_loader.__dict__['sampler']
        )

        databunch.train = new_data_loader
