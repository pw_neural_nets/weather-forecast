from experiments import experiments


def create_evaluation(result):
    evaluation_experiment = experiments.Experiment.build_evaluation(
        trained_model=result.model,
        data_preprocessor=result.preprocessor_data,
        target_preprocessor=result.preprocessor_target,
        **result.exp_params.return_parameters(
            ignore_parameters=['enc_name', 'pos_cls_sampling_mult', 'preprocessing_name'])
    )
    return evaluation_experiment.run_eval()
