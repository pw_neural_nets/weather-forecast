import functools

import torch
from torch import nn, optim

import config
from experiments.models import big_models
from experiments.models import baselines
from experiments.models import small_models
from experiments.models import autoencoder
from experiments.models import shared_weights_models
from experiments.metric_recorders import recorders
from experiments.datasets import datasets


def get_metrics_recorder(mode):
    if mode == 'cls':
        return recorders.MetricsRecorder(mode='cls')
    elif mode == 'reg':
        return recorders.MetricsRecorder(mode='reg')
    else:
        raise ValueError


model_bundle = {
    'shared_day_reg': {
        'dataset': datasets.TempDataset,
        'model': shared_weights_models.SharedDayWeights,
        'loss_fn': nn.MSELoss(),
        'optimizer_fn': optim.SGD,
        'metrics_recorder_getter': functools.partial(get_metrics_recorder, mode='reg'),
        'callbacks': {},
        'model_parameters': {
            'dropout': 0,
            'shared_weights_n_layers': 3,
            'common_n_layers': 4,
            'total_days': config.basic_nb_train_dates,
            'shared_block_shrink_factor': 2,
        },
    },
    'shared_feature_reg': {
        'dataset': datasets.TempDataset,
        'model': shared_weights_models.SharedFeatureWeights,
        'loss_fn': nn.MSELoss(),
        'optimizer_fn': optim.SGD,
        'metrics_recorder_getter': functools.partial(get_metrics_recorder, mode='reg'),
        'callbacks': {},
        'model_parameters': {
            'dropout': 0,
            'shared_weights_n_layers': 3,
            'common_n_layers': 7,
            'total_days': config.basic_nb_train_dates,
            'shared_block_shrink_factor': 2,
            'hours_per_day_part': config.encoding_hours_per_day_part
        },
    },
    'autoencoder': {
        'dataset': datasets.BasicDataset,
        'model': autoencoder.AutoEncoder,
        'loss_fn': nn.MSELoss(),
        'optimizer_fn': optim.SGD,
        'metrics_recorder_getter': functools.partial(get_metrics_recorder, mode='reg'),
        'callbacks': {},
        'model_parameters': {'n_layers': 1, 'compression_multiplier': 1.1, 'dropout': 0},
    },
    'big_cls': {
        'dataset': datasets.WindDataset,
        'model': big_models.ShrinkingBinWindClsNet,
        'loss_fn': nn.CrossEntropyLoss(),
        'optimizer_fn': optim.SGD,
        'metrics_recorder_getter': functools.partial(get_metrics_recorder, mode='cls'),
        'callbacks': {'on_batch_begin': lambda x, y: (x, y.reshape(-1).type(torch.LongTensor))},
        'model_parameters': {'dropout': 0.1},
    },
    'big_reg': {
        'dataset': datasets.TempDataset,
        'model': big_models.ShrinkingTempRegressor,
        'loss_fn': nn.MSELoss(),
        'optimizer_fn': optim.SGD,
        'metrics_recorder_getter': functools.partial(get_metrics_recorder, mode='reg'),
        'callbacks': {},
        'model_parameters': {'dropout': 0.1},
    },
    'basic_cls': {
        'dataset': datasets.WindDataset,
        'model': small_models.BinWindClsNet,
        'loss_fn': nn.CrossEntropyLoss(),
        'optimizer_fn': optim.SGD,
        'metrics_recorder_getter': functools.partial(get_metrics_recorder, mode='cls'),
        'callbacks': {'on_batch_begin': lambda x, y: (x, y.reshape(-1).type(torch.LongTensor))},
        'model_parameters': {},
        },
    'basic_reg': {
        'dataset': datasets.TempDataset,
        'model': small_models.TempRegressor,
        'loss_fn': nn.MSELoss(),
        'optimizer_fn': optim.SGD,
        'metrics_recorder_getter': functools.partial(get_metrics_recorder, mode='reg'),
        'callbacks': {},
        'model_parameters': {'dropout': 0.2},
    },
    'celcius_reg': {
        'dataset': datasets.TempDataset,
        'model': small_models.TempRegressor,
        'loss_fn': nn.L1Loss(),
        'optimizer_fn': optim.SGD,
        'metrics_recorder_getter': functools.partial(get_metrics_recorder, mode='reg'),
        'callbacks': {},
        'model_parameters': {},
    },
    'baseline_mean_cls': {
        'dataset': datasets.WindDataset,
        'model': baselines.BaselineMeanWind,
        'loss_fn': nn.CrossEntropyLoss(),
        'optimizer_fn': optim.SGD,
        'metrics_recorder_getter': functools.partial(get_metrics_recorder, mode='cls'),
        'callbacks': {'on_batch_begin': lambda x, y: (x, y.reshape(-1).type(torch.LongTensor))},
        'model_parameters': {},
    },
    'baseline_mean_reg': {
        'dataset': datasets.TempDataset,
        'model': baselines.BaselineMeanTemp,
        'loss_fn': nn.MSELoss(),
        'optimizer_fn': optim.SGD,
        'metrics_recorder_getter': functools.partial(get_metrics_recorder, mode='reg'),
        'callbacks': {},
        'model_parameters': {},
    },
    'baseline_last_day_cls': {
        'dataset': datasets.WindDataset,
        'model': baselines.BaselineLastWind,
        'loss_fn': nn.CrossEntropyLoss(),
        'optimizer_fn': optim.SGD,
        'metrics_recorder_getter': functools.partial(get_metrics_recorder, mode='cls'),
        'callbacks': {'on_batch_begin': lambda x, y: (x, y.reshape(-1).type(torch.LongTensor))},
        'model_parameters': {},
    },
    'baseline_last_day_reg': {
        'dataset': datasets.TempDataset,
        'model': baselines.BaselineLastTemp,
        'loss_fn': nn.MSELoss(),
        'optimizer_fn': optim.SGD,
        'metrics_recorder_getter': functools.partial(get_metrics_recorder, mode='reg'),
        'callbacks': {},
        'model_parameters': {},
    },
}
