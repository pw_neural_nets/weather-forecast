from typing import List
from dataclasses import dataclass
import itertools

import pandas as pd
import random
import torch
import tqdm
import numpy as np

import config
from experiments.datasets import datasets
from experiments import model_bundles
from experiments.models import model_basic
from experiments.schedulers import lr_schedulers
from experiments.schedulers import batch_size_schedulers
from data_preprocessing.preprocessing import preprocessing


class Experiment:
    bs_scheduler = None

    def __init__(
            self, model, optimizer_fn, databunch, loss_fn, callbacks,
            metrics_recorder, epochs, seed, scheduler_name, bs, **kwargs
    ):

        self.databunch = databunch
        self.loss_fn = loss_fn
        self.callbacks = callbacks
        self.metrics_recorder = metrics_recorder
        self.model = model
        self.optimizer = optimizer_fn(model.parameters(), **kwargs)
        self.scheduler_name = scheduler_name
        self.epochs = epochs
        self.seed = seed
        self.lr_scheduler = lr_schedulers.lr_schedulers[scheduler_name](
            optimizer=self.optimizer,
            lr=kwargs['lr'],
            steps_per_epoch=len(databunch.train) if databunch.train else 1000,
            epochs=epochs,
        )
        if isinstance(bs, list):
            self.bs_scheduler = batch_size_schedulers.BatchScheduler(epochs_bs=bs)

        self.test = False if databunch.test is None else True

    def run_eval(self):
        self.model.eval()
        self.run_epoch(mode='eval')
        self.metrics_recorder.next_epoch()
        return self.metrics_recorder.get_metrics()

    def run_experiment(self):
        np.random.seed(self.seed)
        random.seed(self.seed)
        torch.manual_seed(self.seed)
        self.train_model()
        return self.metrics_recorder.get_metrics()

    def train_model(self):
        self.model.train()
        for epoch in range(self.epochs):
            self.metrics_recorder.next_epoch()
            if self.bs_scheduler: self.bs_scheduler.step(databunch=self.databunch)
            self.run_epoch(mode='train')
            self.model.eval()
            if self.test: self.run_epoch(mode='test')
            self.model.train()

    def run_epoch(self, mode):
        for x, y in self.databunch[mode]:
            if 'on_batch_begin' in self.callbacks:
                x, y = self.callbacks['on_batch_begin'](x, y)

            out = self.model.forward(x)
            loss = self.loss_fn(out, y)
            self.metrics_recorder.next_batch(
                preds=out,
                y=y,
                loss=loss,
                mode=mode
            )

            if mode == 'train':
                self.optimizer.zero_grad()
                loss.backward()
                self.optimizer.step()
                self.lr_scheduler.step()

    def get_pos_class_weight(self):
        return self.databunch.get_pos_class_weight()

    @classmethod
    def from_parameters(
            cls,
            bs,
            encoded_df_path,
            city,
            drop_columns,
            target_cols,
            preprocessing_name,
            model_name,
            enc_name,
            test_size=None,
            **kwargs
    ):
        model_bundle = model_bundles.model_bundle[model_name]

        databunch = datasets.DataBunch.from_config(
            dataset_cls=model_bundle['dataset'],
            df=cls.load_encoded_data(encoded_df_path, city),
            drop_columns=drop_columns,
            preprocessing_name=preprocessing_name,
            bs=bs,
            target_cols=target_cols,
            test_size=test_size,
            city=city,
            enc_name=enc_name,
        )

        baseline_model_dict = cls.get_baseline_model_params(databunch) if cls.is_baseline_cls_model(model_name) else {}
        model = model_bundle['model'].from_config(
            features_count=databunch.get_features_count(),
            target_cols=target_cols,
            **model_bundle['model_parameters'],
            **baseline_model_dict
        )

        return cls(
            databunch=databunch,
            loss_fn=model_bundle['loss_fn'],
            callbacks=model_bundle['callbacks'],
            optimizer_fn=model_bundle['optimizer_fn'],
            metrics_recorder=model_bundle['metrics_recorder_getter'](),
            bs=bs,
            model=model,
            **kwargs
        )

    @classmethod
    def build_evaluation(
            cls,
            encoded_df_path,
            trained_model,
            data_preprocessor,
            target_preprocessor,
            model_name,
            bs,
            target_cols,
            city,
            drop_columns,
            **kwargs
    ):
        model_bundle = model_bundles.model_bundle[model_name]
        evaluation_df_path = encoded_df_path.replace('train', 'test')

        databunch = datasets.DataBunch.get_evaluation(
            dataset_cls=model_bundle['dataset'],
            df=cls.load_encoded_data(evaluation_df_path, city),
            bs=config.eval_batch_size,
            target_cols=target_cols,
            drop_columns=drop_columns,
            data_preprocessor=data_preprocessor,
            target_preprocessor=target_preprocessor,
        )

        return cls(
            databunch=databunch,
            loss_fn=model_bundle['loss_fn'],
            callbacks=model_bundle['callbacks'],
            optimizer_fn=model_bundle['optimizer_fn'],
            metrics_recorder=model_bundle['metrics_recorder_getter'](),
            model=trained_model,
            bs=bs,
            **kwargs
        )

    @classmethod
    def get_baseline_model_params(cls, databunch):
        """Cls baseline model is an exception and needs to know a scaled threshold to be able to predict"""
        return {'preprocessor_target': databunch.test.dataset.preprocessor_target}

    @classmethod
    def load_encoded_data(cls, encoded_df_path, city):
        df = pd.read_csv(encoded_df_path)
        if city is not None:
            df = df[df.City == city]
        if df.empty:
            raise ValueError(f'There are no data points for city {city} from enc_path {encoded_df_path}')
        return df

    @staticmethod
    def is_baseline_cls_model(model_name):
        return model_name == 'baseline_last_day_cls' or model_name == 'baseline_mean_cls'


class ExperimentsGrid:
    def __init__(
        self,
        lr,
        momentum,
        bs,
        enc_parameters,
        cities,
        epochs,
        drop_cols_sets,
        preprocessing_names,
        model_parameters,
        max_exp_retries,
        exp_loss_rejection_coeff=config.exp_loss_rejection_coeff,
    ):
        self.constant_experiment_parameters = {
            'lr': lr,
            'momentum': momentum,
            'bs': bs,
            'epochs': epochs,
        }

        self.lr = lr
        self.momentum = momentum
        self.bs = bs
        self.enc_parameters = enc_parameters
        self.cities = cities
        self.epochs = epochs
        self.drop_cols_sets = drop_cols_sets
        self.preprocessing_names = preprocessing_names
        self.model_parameters = model_parameters
        self.max_exp_retries = max_exp_retries
        self.exp_loss_rejection_coeff = exp_loss_rejection_coeff

    def run_experiments(self, seed_offset, **kwargs):
        results = ExperimentsResults([
            self.get_valid_experiment_result(seed_offset, *exp_params, **kwargs)
            for exp_params in tqdm.tqdm(self.get_grid_parameters_combinations(), desc='experiments')
        ])
        return results

    def get_valid_experiment_result(
            self, seed_offset, model_parameters, enc_parameters,
            preprocessing_name, drop_columns, city, **kwargs):
        for seed in range(self.max_exp_retries):
            experiment_params = ExperimentParameters(
                preprocessing_name=preprocessing_name,
                city=city,
                seed=seed_offset + seed,
                drop_columns=drop_columns,
                **enc_parameters,
                **model_parameters,
                **self.constant_experiment_parameters,
            )

            experiment = Experiment.from_parameters(
                **experiment_params.return_parameters(
                    ignore_parameters=['enc_name', 'pos_cls_sampling_mult']),
                **kwargs
            )
            experiment_params.pos_cls_sampling_mult = experiment.get_pos_class_weight()
            exp_result = experiment.run_experiment()

            if self.is_converged_exp_result(exp_result):
                return ExperimentResults.from_exp(
                    exp_params=experiment_params,
                    results=exp_result,
                    model=experiment.model,
                    preprocessor_data=experiment.databunch.train.dataset.preprocessor_data,
                    preprocessor_target=experiment.databunch.train.dataset.preprocessor_target,
                )

        return ExperimentResults(
            exp_params=experiment_params,
            loss=[np.inf]*self.constant_experiment_parameters['epochs'],
            metrics=[-np.inf]*self.constant_experiment_parameters['epochs'],
            train_loss=[np.inf]*self.constant_experiment_parameters['epochs'],
            train_metrics=[-np.inf]*self.constant_experiment_parameters['epochs']
        )

    def get_grid_parameters_combinations(self):
        return list(itertools.product(
            self.model_parameters,
            self.enc_parameters,
            self.preprocessing_names,
            self.drop_cols_sets,
            self.cities,
        ))

    def is_converged_exp_result(self, exp_result):
        return exp_result['train'].losses[0] > min(exp_result['train'].losses) * self.exp_loss_rejection_coeff


@dataclass(unsafe_hash=True)
class ExperimentParameters:
    lr: float
    momentum: float
    bs: int
    encoded_df_path: str
    city: str
    drop_columns: list
    target_cols: list
    epochs: int
    preprocessing_name: str
    model_name: str
    enc_name: str
    seed: int
    scheduler_name: str
    pos_cls_sampling_mult: float = None

    def return_parameters(self, ignore_parameters):
        return {k: v for k, v in self.__dict__.items() if k not in ignore_parameters}

    def get_parameters_string(self):
        return '_'.join([str(v) for k, v in self.__dict__.items() if k != 'seed'])


@dataclass(unsafe_hash=True)
class ExperimentResults:
    exp_params: ExperimentParameters
    loss: list
    metrics: list
    train_loss: list
    train_metrics: list
    model: model_basic.Model = None
    preprocessor_data: preprocessing.Preprocessor = None
    preprocessor_target: preprocessing.Preprocessor = None

    @classmethod
    def from_exp(cls, exp_params, results, model, preprocessor_data, preprocessor_target):
        losses = results['test'].losses
        metrics = results['test'].metrics
        train_loss = results['train'].losses
        train_metrics = results['train'].metrics
        return cls(
            exp_params=exp_params,
            loss=losses,
            metrics=metrics,
            train_loss=train_loss,
            train_metrics=train_metrics,
            model=model,
            preprocessor_data=preprocessor_data,
            preprocessor_target=preprocessor_target,
        )


@dataclass(unsafe_hash=True)
class ExperimentsResults:
    exps_results: List[ExperimentResults]

    def __getitem__(self, item):
        return self.exps_results[item]

    def __len__(self):
        return len(self.exps_results)

    def get_exps_sorted_by_parameters(self):
        return sorted(self.exps_results, key=lambda x: x.exp_params.get_parameters_string())

    def group_experiments_by_parameters(self):
        sorted_exps = self.get_exps_sorted_by_parameters()
        return [list(g) for k, g in itertools.groupby(sorted_exps, key=lambda x: x.exp_params.get_parameters_string())]

    def get_parameter_values(self, key):
        return set([exp_result.exp_params.__dict__[key] for exp_result in self.exps_results])

    def filter_by_parameter(self, key, val):
        """if iteration is in order then this should return filtered experiments in correct order"""
        return ExperimentsResults(
            exps_results=[
                exp_result for exp_result in self.exps_results
                if exp_result.exp_params.__dict__[key] == val]
            )
    
    def merge_with(self, results):
        self.exps_results.extend(results.exps_results)
