from torch import nn

from experiments.models import model_basic


class ShrinkingNet(model_basic.Model):
    def __init__(self, input_shape, output_shape, dropout):
        super().__init__(input_shape, output_shape)
        shape = input_shape
        repeat = 1
        self.dense_layers = nn.ModuleList([])
        while shape > output_shape * 2:
            self.dense_layers.append(model_basic.DenseBlock(
                input_shape=shape,
                output_shape=shape - shape//2,
                dropout=dropout)
            )
            shape = shape // 2

            for i in range(repeat):
                if not shape > output_shape * 2:
                    break
                self.dense_layers.append(model_basic.DenseBlock(input_shape=shape, output_shape=shape - 10, dropout=0))
                shape -= 10
                repeat += 1
        self.dense_layers.append(nn.Linear(shape, output_shape))

    def forward(self, x):
        for layer in self.dense_layers:
            x = layer(x)
        return x


class ShrinkingBinWindClsNet(ShrinkingNet):
    def predict(self, x):
        y = self.forward(x)
        return y.argmax().numpy()[0]

    @classmethod
    def get_output_shape(cls, shape):
        assert shape == 1, 'Wrong input for binary classification'
        return 2


class ShrinkingTempRegressor(ShrinkingNet):
    @classmethod
    def get_output_shape(cls, shape):
        return shape
