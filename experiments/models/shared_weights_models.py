import torch

from experiments.models import model_basic


class SharedWeights(model_basic.Model):
    """
    1. use shared weights_block as autoencoder for single day
    2. transfer weights from encoder
    3. (optional) add extra later on top of encoder layers with normal lr
    4. use smaller learning rate on shared_weights_block

    idea based on https://arxiv.org/pdf/1708.06742.pdf"""
    def __init__(
            self,
            input_shared_shape,
            n_shared_blocks,
            output_shape,
            shared_block_shrink_factor,
            shared_weights_n_layers,
            common_n_layers,
            dropout,
    ):
        super().__init__(input_shared_shape, output_shape)
        output_shared_shape = input_shared_shape // shared_block_shrink_factor

        self.shared_weights_block = model_basic.DenseLayers(
            input_shape=input_shared_shape,
            output_shape=output_shared_shape,
            n_layers=shared_weights_n_layers,
            dropout=dropout
        )

        self.common_layers = model_basic.DenseLayers(
            input_shape=n_shared_blocks * output_shared_shape,
            output_shape=output_shape,
            n_layers=common_n_layers,
            dropout=dropout,
        )

    def forward(self, groups):
        preprocessed_groups = []
        for group in groups:
            preprocessed_groups.append(self.shared_weights_block.forward(group))
        preprocessed_data = torch.stack(preprocessed_groups)
        preprocessed_data = preprocessed_data.permute(1, 0, 2)
        batch_size = preprocessed_data.shape[0]
        preprocessed_data = preprocessed_data.contiguous().view(batch_size, -1)
        out = self.common_layers.forward(preprocessed_data)
        return out

    @classmethod
    def get_output_shape(cls, shape):
        return shape


class SharedDayWeights(SharedWeights):
    """splits input into days data"""
    def __init__(self, input_shape, total_days, **kwargs):
        self.total_days = total_days

        input_shared_shape = input_shape // total_days
        n_shared_blocks = total_days

        super().__init__(input_shared_shape, n_shared_blocks, **kwargs)

    def forward(self, batch_x):
        # [batch, 5day_features] -> [days, batch, day_features]
        batch_x = torch.split(batch_x, batch_x.shape[1] // self.total_days, dim=1)
        batch_x = torch.stack(batch_x)
        out = super().forward(batch_x)
        return out


class SharedFeatureWeights(SharedWeights):
    """splits input into groups of features for each day"""
    def __init__(self, input_shape, total_days, hours_per_day_part, **kwargs):
        self.total_days = total_days
        self.parts_of_day = 24 // hours_per_day_part

        input_shared_shape = total_days * self.parts_of_day
        n_shared_blocks = input_shape // input_shared_shape

        super().__init__(input_shared_shape, n_shared_blocks, **kwargs)

    def forward(self, batch_x):
        # [batch, 5day_features] -> [days, batch, day_features]
        batch_x = torch.split(batch_x, batch_x.shape[1] // self.total_days, dim=1)
        batch_x = torch.stack(batch_x)

        assert not batch_x[0][0].shape[0] % self.parts_of_day, \
            'Make sure current config settings are ' \
            'the same as the ones used for encoding.'

        # [days, batch, day_features] -> [days, pod, batch, feature_type]
        batch_x = torch.stack(torch.split(
            batch_x,
            batch_x[0][0].shape[0] // self.parts_of_day, dim=2), dim=1
        )
        # [days, pod, batch, feature_type] -> [time, batch, feature_type]
        batch_x = batch_x.contiguous().view(-1, *batch_x.shape[2:])
        # [time, batch, feature_type] -> [feature_type, batch, time]
        batch_x = batch_x.permute(2, 1, 0)

        out = super().forward(batch_x)
        return out
