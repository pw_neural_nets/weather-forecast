from torch import nn
import torch.nn.functional as F

from experiments.models import model_basic


class SmallNet(model_basic.Model):
    def __init__(self, input_shape, output_shape, **kwargs):
        super().__init__(input_shape, output_shape)
        self.fc1 = nn.Linear(input_shape, 20)
        self.fc2 = nn.Linear(20, 15)
        self.fc3 = nn.Linear(15, 5)
        self.fc4 = nn.Linear(5, output_shape)

    def forward(self, x):
        x = F.relu(self.fc1(x))
        x = F.relu(self.fc2(x))
        x = F.relu(self.fc3(x))
        x = self.fc4(x)
        return x


class BinWindClsNet(SmallNet):
    def predict(self, x):
        y = self.forward(x)
        return y.argmax().numpy()[0]

    @classmethod
    def get_output_shape(cls, shape):
        assert shape == 1, 'Wrong input for binary classification'
        return 2


class TempRegressor(SmallNet):
    @classmethod
    def get_output_shape(cls, shape):
        return shape
