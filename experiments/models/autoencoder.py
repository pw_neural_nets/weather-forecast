from experiments.models import model_basic


class AutoEncoder(model_basic.Model):
    def __init__(self, input_shape, output_shape, n_layers, compression_multiplier, dropout):
        super().__init__(input_shape, input_shape)
        encoded_size = int(input_shape / compression_multiplier)

        self.encoder_layers = model_basic.DenseLayers(
            input_shape=input_shape,
            output_shape=encoded_size,
            n_layers=n_layers,
            dropout=dropout,
        )

        self.decoder_layers = model_basic.ExpandingDenseLayers(
            input_shape=encoded_size,
            output_shape=input_shape,
            n_layers=n_layers,
            dropout=dropout,
        )

    def forward(self, x):
        encoded_input = self.encoder_layers.forward(x)
        decoded_input = self.decoder_layers.forward(encoded_input)
        return decoded_input

    @classmethod
    def get_output_shape(cls, shape):
        return shape
