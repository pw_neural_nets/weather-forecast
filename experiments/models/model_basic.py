from torch import nn


class Model(nn.Module):
    def __init__(self, input_shape, output_shape, **kwargs):
        super().__init__()
        self.input_shape = input_shape
        self.output_shape = output_shape

    @classmethod
    def from_config(cls, features_count, target_cols, **kwargs):
        output_shape = cls.get_output_shape(len(target_cols))
        return cls(input_shape=features_count, output_shape=output_shape, **kwargs)

    @classmethod
    def get_output_shape(cls, shape):
        raise NotImplementedError


class DenseBlock(nn.Module):
    def __init__(self, input_shape, output_shape, dropout):
        super().__init__()
        self.dense = nn.Linear(input_shape, output_shape)
        self.dropout = nn.Dropout(p=dropout)
        self.relu = nn.ReLU()

    def forward(self, x):
        x = self.dense(x)
        x = self.dropout(x)
        x = self.relu(x)
        return x


class DenseLayers(nn.Module):
    def __init__(self, input_shape, output_shape, n_layers, dropout):
        super().__init__()
        dense_layers = nn.ModuleList([])
        assert output_shape < input_shape, "input_shape can't smaller than output_shape"
        step = (input_shape - output_shape) // n_layers
        shape = input_shape
        while shape > output_shape + step:
            dense_layers.append(DenseBlock(shape, shape - step, dropout))
            shape -= step
        dense_layers.append(DenseBlock(shape, output_shape, dropout))
        self.dense_layers = dense_layers

    def forward(self, x):
        for layer in self.dense_layers:
            x = layer(x)
        return x


class ExpandingDenseLayers(nn.Module):
    def __init__(self, input_shape, output_shape, n_layers, dropout):
        super().__init__()
        self.dense_layers = nn.ModuleList([])
        assert output_shape > input_shape, "can't expand when output_shape smaller than input_shape"
        step = (output_shape - input_shape) // n_layers
        shape = input_shape
        while shape < output_shape - step:
            self.dense_layers.append(DenseBlock(shape, shape + step, dropout))
            shape += step
        self.dense_layers.append(DenseBlock(shape, output_shape, dropout))

    def forward(self, x):
        for layer in self.dense_layers:
            x = layer(x)
        return x

