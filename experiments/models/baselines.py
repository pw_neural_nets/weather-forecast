import numpy as np
import torch
from torch import nn

import config
from experiments.models import model_basic


class BaselineModel(model_basic.Model):
    def __init__(self, preprocessor_target=None, **kwargs):
        super().__init__(**kwargs)
        self.measurements_per_day = int(
            len(config.mode['cls']['target_cols']) * self.input_shape / config.basic_nb_train_dates)
        self.scaled_pos_class_cutoff = preprocessor_target.preprocess(
            [[config.pos_class_cutoff] * self.measurements_per_day])[0] if preprocessor_target else None
        self.fc1 = nn.Linear(1, 1)

    @classmethod
    def get_output_shape(cls, shape):
        return []


class BaselineWind(BaselineModel):
    def array_to_tensor(self, array):
        return torch.from_numpy(array).float()

    def target_to_class(self, targets):
        for trg, cutoff in zip(targets, self.scaled_pos_class_cutoff):
            if trg >= cutoff:
                return True
        return False

    def get_target_class(self, targets):
        target_classes = np.array([self.target_to_class(targets)])
        return target_classes

    def get_class_preds(self, y):
        y_cls = self.get_target_class(y)
        y_cls = self.array_to_tensor(y_cls)
        y_cls = y_cls.max().unsqueeze(-1)
        return y_cls


class BaselineMeanWind(BaselineWind):
    def forward(self, x):
        y = x.reshape(x.shape[0], config.basic_nb_train_dates, -1).mean(axis=1)
        preds = [int(self.get_class_preds(y_)) for y_ in y]
        result = np.eye(2)[preds]
        return self.array_to_tensor(result).requires_grad_(True)


class BaselineLastWind(BaselineWind):
    def forward(self, x):
        last_day_idx = int(len(config.mode['reg']['target_cols']) * self.input_shape / config.basic_nb_train_dates)
        y = x[:, -last_day_idx:]
        preds = [int(self.get_class_preds(y_)) for y_ in y]
        result = np.eye(2)[preds]
        return self.array_to_tensor(result).requires_grad_(True)


class BaselineTemp(BaselineModel):
    pass


class BaselineMeanTemp(BaselineTemp):
    def forward(self, x):
        return x.mean(axis=1).unsqueeze(-1).requires_grad_(True)


class BaselineLastTemp(BaselineTemp):
    def forward(self, x):
        return x[:, -self.measurements_per_day:].mean(axis=1).unsqueeze(-1).requires_grad_(True)
