import numpy as np
import pandas as pd
import torch
from torch.utils.data import DataLoader
from torch.utils.data import Dataset
from torch.utils.data import WeightedRandomSampler

import config
from data_preprocessing.preprocessing import preprocessing


def get_dataset_from_name(mode):
    if mode == 'cls':
        return WindDataset
    elif mode == 'reg':
        return WeatherDataset
    else:
        raise ValueError('Mode not supported')


class WeatherDataset(Dataset):
    def __init__(
            self,
            preprocessed_data,
            preprocessed_targets,
            preprocessor_data,
            preprocessor_target,
            measures_per_day,
            **kwargs
    ):
        self.preprocessed_data = preprocessed_data
        self.preprocessed_targets = preprocessed_targets
        self.preprocessor_data = preprocessor_data
        self.preprocessor_target = preprocessor_target
        self.measures_per_day = measures_per_day

    def __len__(self):
        return len(self.preprocessed_data)

    def __getitem__(self, idx):
        x = self.preprocessed_data[idx]
        x = self.array_to_tensor(x)

        y = self.preprocessed_targets[idx]
        y = self.array_to_tensor(y)
        return x, y

    def inv_preprocess_targets(self, targets):
        return self.preprocessor_target.inverse_preprocess(targets)

    def get_class_weights(self):
        return NotImplemented

    @staticmethod
    def array_to_tensor(array):
        return torch.from_numpy(array).float()


class BasicDataset(WeatherDataset):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    def get_class_weights(self):
        return len(self) * [1]

    def __getitem__(self, idx):
        x, y = super().__getitem__(idx)
        return x, y


class TempDataset(WeatherDataset):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

    def get_class_weights(self):
        return len(self) * [1]

    def __getitem__(self, idx):
        x, y = super().__getitem__(idx)
        y = y.mean().unsqueeze(-1)
        return x, y


class WindDataset(WeatherDataset):
    def __init__(
            self,
            preprocessed_data,
            preprocessed_targets,
            preprocessor_data,
            preprocessor_target,
            measures_per_day,
            pos_class_cutoff=config.pos_class_cutoff,
    ):
        super().__init__(
            preprocessed_data,
            preprocessed_targets,
            preprocessor_data,
            preprocessor_target,
            measures_per_day,
        )
        self.scaled_pos_class_cutoff = self.preprocessor_target.preprocess([[pos_class_cutoff]*measures_per_day])[0]

    def __getitem__(self, idx):
        x, y = super().__getitem__(idx)
        y_cls = self.get_target_class(y)
        y_cls = self.array_to_tensor(y_cls)
        y_cls = y_cls.max().unsqueeze(-1)
        return x, y_cls

    def target_to_class(self, targets):
        for trg, cutoff in zip(targets, self.scaled_pos_class_cutoff):
            if trg >= cutoff:
                return True
        return False

    def get_target_class(self, targets):
        target_classes = np.array([self.target_to_class(targets)])
        return target_classes

    def get_pos_class_weight(self, target_classes):
        class_balance = sum(target_classes)/len(self)
        if class_balance == 0:
            pos_class_weight = 1
        elif class_balance > 1/10:
            pos_class_weight = 1/class_balance
        else:
            pos_class_weight = (1/class_balance)**(1/2)
        return pos_class_weight

    def get_class_weights(self):
        target_classes = np.array([1 if y.numpy()[0] == 1 else 0 for x, y in self])
        pos_class_weight = self.get_pos_class_weight(target_classes)
        class_weights = target_classes * pos_class_weight
        class_weights = np.where(class_weights == 0, 1, class_weights)
        return class_weights


class DataBunch:
    def __init__(
        self,
        train_data_loader=None,
        test_data_loader=None,
        unshuffled_train_data_loader=None,
        eval_data_loader=None,
    ):
        self.train = train_data_loader
        self.unshuffled_train = unshuffled_train_data_loader
        self.test = test_data_loader
        self.eval = eval_data_loader

    def __getitem__(self, mode):
        return self.__dict__[mode]

    def get_features_count(self):
        return len(self.train.dataset[0][0])

    def get_pos_class_weight(self):
        return max(self.train.dataset.get_class_weights())

    @classmethod
    def build_train(cls, dataset_cls, bs, **kwargs):
        train_weather_dataset = dataset_cls(**kwargs)
        sampler = WeightedRandomSampler(
            weights=train_weather_dataset.get_class_weights(),
            num_samples=len(train_weather_dataset),
            replacement=True
        )

        train_data_loader = DataLoader(
            train_weather_dataset,
            batch_size=bs,
            num_workers=1,
            sampler=sampler
        )

        unshuffled_train_data_loader = DataLoader(
            train_weather_dataset,
            batch_size=bs,
            shuffle=False,
            num_workers=1
        )

        return train_data_loader, unshuffled_train_data_loader

    @classmethod
    def build_test(cls, dataset_cls, bs, **kwargs):
        test_weather_dataset = dataset_cls(**kwargs)

        test_data_loader = DataLoader(
            test_weather_dataset,
            batch_size=bs,
            shuffle=False,
            num_workers=1
        )

        return test_data_loader

    @classmethod
    def from_config(
            cls,
            dataset_cls,
            df,
            preprocessing_name,
            target_cols,
            bs,
            drop_columns,
            city,
            **kwargs
    ):
        df, measures_per_day, new_drop_cols = cls.infer_data_time_resolution(df=df, drop_columns=drop_columns)

        preprocessor_pipeline = preprocessing.PreprocessingPipeline.build_from_name(
            name=preprocessing_name,
            target_cols=target_cols,
            drop_columns=drop_columns,
            measures_per_day=measures_per_day,
            city=city,
            **kwargs
        )

        pre_data_train, pre_data_test, pre_targets_train, pre_targets_test = \
            preprocessor_pipeline.preprocess(
                df=df,
            )

        train_data_loader, unshuffled_train_data_loader = cls.build_train(
            dataset_cls=dataset_cls,
            preprocessed_data=pre_data_train,
            preprocessed_targets=pre_targets_train,
            bs=bs[0] if isinstance(bs, list) else bs,
            measures_per_day=measures_per_day,
            **preprocessor_pipeline.get_preprocessors()
        )

        test_data_loader = cls.build_test(
            dataset_cls=dataset_cls,
            preprocessed_data=pre_data_test,
            preprocessed_targets=pre_targets_test,
            bs=config.eval_batch_size,
            measures_per_day=measures_per_day,
            **preprocessor_pipeline.get_preprocessors()
        ) if pre_data_test is not None else None

        return cls(
            train_data_loader=train_data_loader,
            test_data_loader=test_data_loader,
            unshuffled_train_data_loader=unshuffled_train_data_loader,
        )

    @classmethod
    def get_evaluation(cls, dataset_cls, df, bs, target_cols, drop_columns, data_preprocessor, target_preprocessor):
        df, measures_per_day, new_drop_cols = cls.infer_data_time_resolution(df=df, drop_columns=drop_columns)

        data_targets_splitter = preprocessing.MultiDayDataTargetsSplitter(
            target_cols=target_cols,
            drop_columns=drop_columns,
            measures_per_day=measures_per_day,
        )

        data, targets = data_targets_splitter.split(df)
        pre_data = data_preprocessor.preprocess(data)
        pre_targets = target_preprocessor.preprocess(targets)

        eval_data_loader = cls.build_eval(
            dataset_cls=dataset_cls,
            preprocessed_data=pre_data,
            preprocessed_targets=pre_targets,
            bs=bs,
            measures_per_day=measures_per_day,
            preprocessor_data=data_preprocessor,
            preprocessor_target=target_preprocessor,
        )
        return cls(eval_data_loader=eval_data_loader)

    @classmethod
    def build_eval(cls, dataset_cls, bs, **kwargs):
        test_weather_dataset = dataset_cls(**kwargs)

        return DataLoader(
            test_weather_dataset,
            batch_size=bs,
            shuffle=False,
            num_workers=1,
        )

    @classmethod
    def infer_data_time_resolution(cls, df, drop_columns):
        """
        Data might have a format of single measurement per day where MultiIndex consists of city|date or
        multiple measurements per day with MultiIndex city|date|part_of_day
        """
        if config.part_of_day_col in df.columns:
            df = df.sort_values(by=[config.city_col, config.time_col, config.part_of_day_col]).reset_index(drop=True)
            measures_per_day = len(pd.unique(df[config.part_of_day_col]))
            new_drop_cols = list(drop_columns + [config.part_of_day_col])
        else:
            measures_per_day = 1
            new_drop_cols = drop_columns
        return df, measures_per_day, new_drop_cols

