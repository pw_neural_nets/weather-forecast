from typing import List, Dict
from dataclasses import dataclass
from collections import namedtuple

import numpy as np
from sklearn.metrics import accuracy_score
from sklearn.metrics import precision_score
from sklearn.metrics import recall_score
from sklearn.metrics import average_precision_score
from sklearn.metrics import f1_score
from sklearn.metrics import confusion_matrix


ConfidenceIntervalPoints = namedtuple('ConfidenceIntervalPoints', 'value, std')


class MetricsRecorder:
    def __init__(self, mode):
        if mode == 'cls':
            self.metrics = {'test': ClsMetrics(), 'train': ClsMetrics(), 'eval': ClsMetrics()}
        elif mode == 'reg':
            self.metrics = {'test': RegMetrics(), 'train': RegMetrics(), 'eval': RegMetrics()}
        else:
            raise ValueError('Mode not supported')

    def __getitem__(self, item):
        return self.metrics[item]

    def __repr__(self):
        desc = f"{self.__class__.__name__}: \n "
        for k, v in self.metrics.items():
            desc += f'\t {k}: {v} \n'
        return desc

    def next_epoch(self):
        for metric in self.metrics.values():
            metric.next_epoch()

    def next_batch(self, mode, preds, y, loss):
        self.metrics[mode].next_batch(preds=preds, y=y, loss=loss)

    def get_metrics(self):
        return self


class RegMetrics:
    def __init__(self):
        self.losses = list()
        self.running_loss = 0
        self.metrics = list()

    def __repr__(self):
        desc = f'{self.__class__.__name__}: \n'
        for k, v in self.__dict__.items():
            if isinstance(v, list):
                if len(v) > 10:
                    attr_str = f'\t {k} {v[:5]}...{v[-5:]} \n'
                else:
                    attr_str = f'\t {k} {v} \n'
                desc += attr_str
        return desc

    def next_epoch(self):
        if self.running_loss:
            self.losses.append(self.running_loss)
            self.metrics.append({'neg_loss': -1 * self.running_loss})
        self.running_loss = 0

    def next_batch(self, preds, y, loss):
        samples_count = len(y)
        self.running_loss += loss.item()/samples_count


class ClsMetrics:
    def __init__(self):
        self.losses = list()
        self.running_loss = 0

        self.metrics = list()
        self._target_labels = list()
        self._pred_labels = list()

    def __repr__(self):
        desc = f'{self.__class__.__name__}: \n'
        for k, v in self.__dict__.items():
            if isinstance(v, list) and k[0] != '_':
                if len(v) > 10:
                    attr_str = f'\t {k} {v[:5]}...{v[-5:]} \n'
                else:
                    attr_str = f'\t {k} {v} \n'
                desc += attr_str
        return desc

    def next_epoch(self):
        if self.running_loss:
            self.losses.append(self.running_loss)

        if self._target_labels and self._pred_labels:
            self.calculate_metrics(
                target_labels=self._target_labels,
                pred_labels=self._pred_labels
            )

        self._target_labels = list()
        self._pred_labels = list()
        self.running_loss = 0

    def next_batch(self, preds, y, loss):
        samples_count = len(y)
        self.running_loss += loss.item()/samples_count

        self._pred_labels += preds.argmax(axis=1).tolist()
        self._target_labels += y.tolist()

    def calculate_metrics(self, target_labels, pred_labels):
        if target_labels == pred_labels:
            print('perfect predictions')
            tn, fp, fn, tp = 0, 0, 0, 0
        else:
            tn, fp, fn, tp = confusion_matrix(target_labels, pred_labels).ravel()
        self.metrics.append(
            {
                'neg_loss': -1 * self.running_loss,
                'acc': accuracy_score(y_true=target_labels, y_pred=pred_labels),
                'prec': precision_score(y_true=target_labels, y_pred=pred_labels),
                'recall': recall_score(y_true=target_labels, y_pred=pred_labels),
                'f1': f1_score(y_true=target_labels, y_pred=pred_labels),
                'avg_prec': average_precision_score(y_true=target_labels, y_score=pred_labels),
                'true_positive': tp,
                'true_negative': tn,
                'false_positive': fp,
                'false_negative': fn,
            }
        )


@dataclass(unsafe_hash=True)
class AvgResultsMetrics:
    metrics: List[Dict[str, ConfidenceIntervalPoints]]
    train_metrics: List[Dict[str, ConfidenceIntervalPoints]]

    @classmethod
    def from_grouped_experiments_results(cls, homogeneous_parameter_exps_results):
        metrics = {}
        # homogeneous_exps_metrics = list(exp_metrics1, exp_metrics2)
        # exp_metrics1.metrics = [step1_dict{ 'name': val, 'name2': val }, step2_dict{ 'name': val, 'name2': val }]

        for metric_type in ['metrics', 'train_metrics']:
            sub_metrics = []
            for exps_result_step in zip(*map(lambda exp_result: exp_result.__dict__[metric_type], homogeneous_parameter_exps_results)):
                # step1_dict{ '1name1': val, 'name2': val }, step1_dict{ '1name2': val, 'name2': val }
                exps_result_step = [x for x in exps_result_step if x != -np.inf]

                if not exps_result_step:
                    continue

                row = {}
                for step_named_metric in zip(*map(lambda step: step.items(), exps_result_step)):
                    # v = (('1name1': val), ('1name2': val))
                    same_names, metric_values = zip(*step_named_metric)

                    mean_value = np.mean(metric_values)
                    std_value = np.std(metric_values)

                    row[same_names[0]] = ConfidenceIntervalPoints(value=mean_value, std=std_value)
                sub_metrics.append(row)
            metrics[metric_type] = sub_metrics

        return cls(**metrics)

