# preprocessings = ['scaler', 'pca', 'stationary']
# models = ['basic_cls', 'basic_reg']

test_size = 0.2
basic_target_date = 7
basic_nb_train_dates = 5
min_wind_speed = 9
min_explained_pca = 0.98
pos_class_cutoff = 9
non_numerical_drop_cols = ['City', 'Time']
exp_loss_rejection_coeff = 1.3
eval_batch_size = 10

mode = {
        'cls': {
                'target_cols': ['WindSpeed'],
        },
        'reg': {
                'target_cols': ['AirTemp'],
        }
}

autoencoder_file_location = './pretrained_autoencoder/autoencoder_model_{city}_{enc_name}.pkl'

time_col = 'Time'
cloud_col = "Clouds"
wind_dir_col = 'WindDirectionAngle'
wind_speed_col = 'WindSpeed'
vis_col = "Visibility"
temp_col = 'AirTemp'

city_col = 'City'
part_of_day_col = "PartOfDay"
ns_wind_col = "WindNS"
we_wind_col = "WindWE"
sin_month_col = 'MonthSin'
cos_month_col = 'MonthCos'

column_names_mapping = {
        'Local time': time_col,
        'T': temp_col,
        'Po': 'PressureAtStation',
        'P': 'PressureMeanSeaLevel',
        'U': 'Humidity',
        'DD': wind_dir_col,
        'Ff': wind_speed_col,
        'N': cloud_col,
        'VV': vis_col,
        'Td': 'DewpointTemp'
}

output_csv_columns = [
        'Lr', 'Momentum', 'Bs', 'Encoding', 'City',
        'Samping Weight', 'Drop Columns', 'Epochs',
        'Preprocessing', 'Model', 'Loss'
]
aggregations_columns = ['Loss max', 'Loss min', 'Loss mean', 'Loss std', 'Num of exp']

cities = {
        'train_1': [
                'Colombo',
                'Buenos Aires',
                'Miami',
                'Port-of-Spain',
                ],
        'train_2': [
                'Rabat',
                'Rome',
                'Saint Petersburg',
                'Santiago',
                'Singapore',
                'Skopje',
                'Washington',
        ]
}


encoding_hours_per_day_part = 8
na_filler_n_iter = 3
allowed_time_series_gap_size = 4
