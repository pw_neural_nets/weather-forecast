import pandas as pd
from pathlib import Path

import config
from data_preprocessing.encoding import encoding_pipelines
from data_preprocessing.encoding import na_fillers


def prepare_data(
        df,
        day_part_hours,
        na_filler_n_iter,
        allowed_time_series_gap_size,
        column_names_mapping,
        preprocessed_csv_path,
        col_names_to_drop,
        check_na_cols,
        month_enc,
        aggregator_name,
):
    df = df.rename(columns=column_names_mapping)

    na_filler = na_fillers.MeanFiller(
        time_col=config.time_col,
        city_col=config.city_col,
    )

    pipeline = encoding_pipelines.DefaultEncodingPipeline.build_default(
        day_part_hours=day_part_hours,
        na_filler_n_iter=na_filler_n_iter,
        allowed_time_series_gap_size=allowed_time_series_gap_size,
        col_names_to_drop=col_names_to_drop,
        check_na_cols=check_na_cols,
        na_filler=na_filler,
        month_enc=month_enc,
        aggregator_name=aggregator_name,
    )

    df = pipeline.pipe_data(df)
    df.to_csv(preprocessed_csv_path)

prefix = ''

for csv_name in ['train_1']:
    for enc_month, enc_month_name in [(True, '_feature_month'), (False, '')]:
        for agg, agg_name in [('daily', '_daily'), ('multidaily', '_tridaily')]:

            target_csv_name = prefix + csv_name + agg_name + enc_month_name
            data_folder = Path('./data/data')
            data_csv_path = data_folder/csv_name

            preprocessed_csv_path = Path('./data/preprocessed/{}.csv'.format(target_csv_name))

            df = pd.read_csv(data_csv_path, sep=';')

            prepare_data(
                df=df,
                day_part_hours=config.encoding_hours_per_day_part,
                na_filler_n_iter=config.na_filler_n_iter,
                allowed_time_series_gap_size=config.allowed_time_series_gap_size,
                column_names_mapping=config.column_names_mapping,
                preprocessed_csv_path=preprocessed_csv_path,
                col_names_to_drop=[config.wind_dir_col],
                check_na_cols=[config.wind_speed_col, config.temp_col],
                month_enc=enc_month,
                aggregator_name=agg,
            )
