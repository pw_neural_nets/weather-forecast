import pandas as pd

df = pd.read_csv('./experiment_results/experiment_grid_results2.csv')

cities = df['City'].unique()
models = df['Model'].unique()

results_path = './experiment_results/best_results_per_city_{}2.csv'

for model in models:
    sub_model_df = df[df['Model'] == model]

    best_models = pd.DataFrame()
    for city in cities:
        sub_city_df = sub_model_df[sub_model_df['City'] == city]
        min_loss_idx = sub_city_df['Loss mean'].idxmin()

        best_row = sub_city_df.loc[min_loss_idx]
        best_models = best_models.append(best_row)

    filled_results_path = results_path.format(model)
    best_models.to_csv(filled_results_path, index=False)
