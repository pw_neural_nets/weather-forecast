import pandas as pd
import numpy as np
file = pd.read_csv("data/projekt_3_dane/test_1", sep=";")
file['Local time'] = pd.to_datetime(file['Local time']).dt.date
stats = []
for city in file.City.unique():
    city_df = file[file.City == city]
    obs_num = len(city_df)
    days_num = len(city_df['Local time'].unique())
    windy_df = city_df[city_df.Ff >= 9]
    windy_num = len(windy_df['Local time'].unique())
    agg_df = city_df.groupby(['Local time']).agg({'T': [np.min, np.max]})
    mean_max_temp = np.mean(agg_df[('T', 'amax')])
    mean_min_temp = np.mean(agg_df[('T', 'amin')])
    stats.append([city, obs_num, days_num, windy_num, mean_max_temp, mean_min_temp])
stats_df = pd.DataFrame(stats, columns=['City', 'Number of obs', 'Number of days', 'Number of windy days', 'Mean max temp', 'Mean min temp'])
print(stats_df)