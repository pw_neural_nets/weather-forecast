import pickle
import random
import torch
import numpy as np

SEED = 3
np.random.seed(SEED)
random.seed(SEED)
torch.manual_seed(SEED)
torch.backends.cudnn.deterministic = True
torch.backends.cudnn.benchmark = False

from experiments import experiments

epochs = 400
constant_experiment_parameters = {
    'lr': 0.01,
    'momentum': 0.9,
    'bs': 10,
    'epochs': epochs,
    'scheduler_name': 'cos_ann',
}

enc_name = 'tridaily_month'
enc_parameters = {
    'enc_name': enc_name, 'encoded_df_path': './data/preprocessed/train_1_tridaily_feature_month.csv'}

model_parameters = {'model_name': 'autoencoder', 'target_cols': []}

city = 'Buenos Aires'
experiment_params = experiments.ExperimentParameters(
    preprocessing_name='train_autoencoder',
    city=city,
    seed=SEED,
    drop_columns=[],
    **enc_parameters,
    **model_parameters,
    **constant_experiment_parameters,
)

experiment = experiments.Experiment.from_parameters(
    **experiment_params.return_parameters(
    ignore_parameters=['pos_cls_sampling_mult']),
)

experiment_params.pos_cls_sampling_mult = experiment.get_pos_class_weight()

experiment.run_experiment()

with open(f'autoencoder_model_{epochs}ep_{city}_{enc_name}.pkl', 'wb') as f:
    pickle.dump(experiment.model, f)

with open(f'autoencoder_exp_{epochs}ep_{city}_{enc_name}.pkl', 'wb') as f:
    pickle.dump(experiment, f)
