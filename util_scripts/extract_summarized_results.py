import pickle

from pathlib import Path

import pandas as pd

from experiments import experiments
from experiments.metric_recorders import recorders
from experiments.visualization import visualization
from experiments.evaluation import evaluation


def get_avg_results(experiments_results):
    grouped_experiments = experiments_results.group_experiments_by_parameters()
    avg_results = []
    for exp_group in grouped_experiments:
        metrics_avg = recorders.AvgResultsMetrics.from_grouped_experiments_results(exp_group)
        if not metrics_avg.metrics:
            print('skip avg')
            continue
        exp = exp_group[0]
        exp.metrics = metrics_avg.metrics
        exp.train_metrics = metrics_avg.train_metrics
        avg_results.append(exp)
    return experiments.ExperimentsResults(avg_results)


city_set = 1
RESULTS_FOLDER = Path('./experiment_results')
RESULTS_PATH = RESULTS_FOLDER/f'results{city_set}.pkl'
target_csv_path = RESULTS_FOLDER/f'train{city_set}_best_configs.csv'
target_img_path = RESULTS_FOLDER/'imgs'
model_to_metric_name = {'basic_cls': 'f1', 'basic_reg': 'neg_loss'}


def main():
    with open(RESULTS_PATH, 'rb') as pickle_file:
        results = pickle.load(pickle_file)

    # 1. aggregate mean and std over whole history of metrics for exp with all the same parameters
    # calculate train, test mean, std of metrics | loss as reg metric and f1 for cls

    exp_results = experiments.ExperimentsResults(results)
    avg_results = get_avg_results(exp_results)


    best_exp_results_per_city_model = []
    # 2. pick experiment with the best metric scores per city
    for city in avg_results.get_parameter_values(key='city'):
        city_results = avg_results.filter_by_parameter(key='city', val=city)
        for model_name in avg_results.get_parameter_values(key='model_name'):
            mode_results = city_results.filter_by_parameter(key='model_name', val=model_name)
            if not mode_results:
                continue

            metric_name = model_to_metric_name[model_name]
            best_exp = max(mode_results, key=lambda exp: max([x[metric_name].value for x in exp.metrics]))
            best_exp_results_per_city_model.append(best_exp)

    # 3. make plots of training for best config per city
    for exp_result in best_exp_results_per_city_model:
        visualization.save_training_plot(exp_result, target_img_path, model_to_metric_name)

    # 4. run eval on best params per city
    # 5. run baseline on best params per city on eval and train data
    df = pd.DataFrame()
    for exp_result in best_exp_results_per_city_model:
        metric_name = model_to_metric_name[exp_result.exp_params.model_name]

        train_metric = max([(m[metric_name].value, m[metric_name].std) for m in exp_result.train_metrics])
        metric = max([(m[metric_name].value, m[metric_name].std) for m in exp_result.metrics])

        train_vals, train_std = train_metric
        test_vals, test_std = metric

        try:
            eval_result = evaluation.create_evaluation(exp_result)
            eval_score = abs(max([x[metric_name] for x in eval_result['eval'].metrics]))
        except Exception as ee:
            print(ee)
            eval_score = -1

        baseline_experiment = experiments.Experiment.from_parameters(
            lr=0,
            momentum=0,
            epochs=2,
            model_name='baseline_last_day_' + exp_result.exp_params.model_name[-3:],
            **exp_result.exp_params.return_parameters(
                ignore_parameters=['enc_name', 'pos_cls_sampling_mult', 'model_name', 'lr', 'momentum', 'epochs']
            )
        )

        bl_exp_results = baseline_experiment.run_experiment()
        baseline_result = abs(max([x[metric_name] for x in bl_exp_results['train'].metrics]))

        exp_type = exp_result.exp_params.model_name[-3:]

        try:
            bl_evaluation_experiment = experiments.Experiment.from_parameters(
                model_name='baseline_last_day_' + exp_type,
                encoded_df_path=exp_result.exp_params.encoded_df_path.replace('train', 'test'),
                test_size=0.02,
                epochs=2,
                **exp_result.exp_params.return_parameters(
                    ignore_parameters=['enc_name', 'pos_cls_sampling_mult', 'model_name', 'encoded_df_path', 'epochs'])
            )

            bl_eval_exp_results = bl_evaluation_experiment.run_experiment()
            eval_baseline_score = abs(max([x[metric_name] for x in bl_eval_exp_results['train'].metrics]))
        except Exception as ee:
            print(ee)
            eval_baseline_score = -1


        # 6. create baseline plots and confusion_matrix
        visualization.save_plots(
            bl_exp_results, baseline_experiment, target_img_path/"baseline",
            type_=exp_type, is_baseline=True, exp_result=exp_result)

        model_exp = experiments.Experiment.from_parameters(
            **exp_result.exp_params.return_parameters(
                ignore_parameters=['enc_name', 'pos_cls_sampling_mult']),
        )

        model_exp.model = exp_result.model

        visualization.save_plots(
            exp_result, model_exp, target_img_path/"model",
            type_=exp_type, is_baseline=False, exp_result=exp_result)

        # 7. save to csv
        row = {
            'City': exp_result.exp_params.city,
            'Encoding': exp_result.exp_params.enc_name,
            'Preprocessing': exp_result.exp_params.preprocessing_name,
            'Samping Weight': exp_result.exp_params.pos_cls_sampling_mult,
            'Metric Train': abs(train_vals),
            'Metric Test': abs(test_vals),
            'Metric Eval': eval_score,
            'Baseline Train': baseline_result,
            'Baseline Eval': eval_baseline_score,
            'Metric Std Train': train_std,
            'Metric Std Test': test_std,
            'Experiment type': exp_type,
        }

        df = df.append([row])

    df.to_csv(target_csv_path, index=False)

main()