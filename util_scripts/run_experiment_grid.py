def warn(*args, **kwargs):
    pass
import warnings
warnings.warn = warn

import os
import pickle
import pandas as pd
from pathlib import Path
import tqdm
pd.set_option('display.max_columns', 500)

from experiments import experiments
import random
import torch
import numpy as np

torch.backends.cudnn.deterministic = True
torch.backends.cudnn.benchmark = False
np.random.seed(1)
random.seed(1)
torch.manual_seed(1)

RESULTS_FOLDER = Path('./experiment_results')
RESULTS_PATH = RESULTS_FOLDER/'results1.pkl'

def run_experiment_grid():
    enc_parameters = [
        # {'enc_name': 'tridaily', 'encoded_df_path': './data/preprocessed/train_2_tridaily.csv'},
        # {'enc_name': 'default', 'encoded_df_path': './data/preprocessed/train_2_daily.csv'},
        # {'enc_name': 'month', 'encoded_df_path': './data/preprocessed/train_2_daily_feature_month.csv'},
        # {'enc_name': 'tridaily_month', 'encoded_df_path': './data/preprocessed/train_2_tridaily_feature_month.csv'},
        # {'enc_name': 'tridaily', 'encoded_df_path': './data/preprocessed/train_1_tridaily.csv'},
        # {'enc_name': 'default', 'encoded_df_path': './data/preprocessed/train_1_daily.csv'},
        {'enc_name': 'month', 'encoded_df_path': './data/preprocessed/train_1_daily_feature_month.csv'},
        # {'enc_name': 'tridaily_month', 'encoded_df_path': './data/preprocessed/train_1_tridaily_feature_month.csv'}
    ]

    max_exp_retries = 1
    lr = 0.01
    momentum = 0.9
    bs = 10
    epochs = 60
    single_experiment_start_seeds = [0, 100, 200]

    cities = [
        'Buenos Aires',
        # 'Colombo',
        # 'Miami',
        # 'Port-of-Spain',
        # 'Rabat',
        # 'Rome',
        # 'Saint Petersburg',
        # 'Santiago',
        # 'Singapore',
        # 'Skopje',
        # 'Washington',
        # None
    ]

    preprocessing_names = [
        'scaler',
        # 'pca',
        # 'stationary'
    ]

    model_parameters = [
        {'model_name': 'basic_reg', 'target_cols': ['AirTemp']},
        # {'model_name': 'celcius_reg', 'target_cols': ['AirTemp']},
        # {'model_name': 'basic_cls', 'target_cols': ['WindSpeed']}
    ]

    drop_cols_sets = [[]]  # ['City Port Moresby', 'City Colombo', 'City Male', 'City Buenos Aires', 'City Port-of-Spain', 'City Miami', 'City Lima', 'City Singapore', 'City Skopje', 'City Rome', 'City Rabat', 'City Saint Petersburg', 'City Santiago', 'City Washington']]

    for enc in enc_parameters:
        fname = enc['encoded_df_path'].replace('train', 'test')
        assert os.path.isfile(fname), f'Evaluation csv {fname} that corresponds' \
                                      f' to one of provided training csv files is missing'

    experiment_grid = experiments.ExperimentsGrid(
        lr=lr, momentum=momentum, bs=bs, enc_parameters=enc_parameters,
        epochs=epochs, drop_cols_sets=drop_cols_sets, cities=cities,
        preprocessing_names=preprocessing_names,
        model_parameters=model_parameters, max_exp_retries=max_exp_retries
    )

    results = experiments.ExperimentsResults([])
    for seed_offset in tqdm.tqdm(single_experiment_start_seeds, desc='seeded_grid'):
        results.merge_with(experiment_grid.run_experiments(seed_offset))

    # result_df = egm.to_dataframe(results)
    # print(result_df)

    with open(RESULTS_PATH, 'wb') as pickle_file:
        pickle.dump(results, pickle_file)

    # result_df.to_csv(TARGET_FOLDER/'experiment_grid_results1.csv', index=False)


run_experiment_grid()
