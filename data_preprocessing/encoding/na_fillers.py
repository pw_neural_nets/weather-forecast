import numpy as np
import pandas as pd

import statsmodels.imputation.mice as mice


class NAFiller:
    """Base class for objects that fills missing values"""
    def __init__(self, time_col, city_col):
        self.time_col = time_col
        self.city_col = city_col

    def fill_na(self, df):
        return NotImplemented

    def get_numeric_columns(self, df):
        return df.select_dtypes(include=np.number).columns.values


class MICEFiller(NAFiller):
    def __init__(self, n_iter, time_col, city_col):
        super().__init__(time_col=time_col, city_col=city_col)
        self.n_iter = n_iter

    def fill_na(self, df):
        df, idx_cols = self.flatten_df(df)
        df = self.get_filled_na_numeric(df)
        if idx_cols[0] is not None:
            df = df.set_index(idx_cols)
        return df

    def flatten_df(self, df):
        idx_cols = df.index.names
        if idx_cols[0] is not None:
            df = df.reset_index()
        return df, idx_cols

    def get_filled_na_numeric(self, df):
        numeric_cols = self.get_numeric_columns(df)
        df_numeric = df[numeric_cols]
        imp = mice.MICEData(df_numeric)
        imp.update_all(n_iter=self.n_iter)
        df_numeric = pd.DataFrame(imp.data)
        df.loc[:, numeric_cols] = df_numeric
        return df


class NearDayFiller(NAFiller):
    def fill_na(self, df):
        numeric_cols = self.get_numeric_columns(df)
        df_numeric = df[numeric_cols]

        while df.isnull().sum().sum():
            df_bfilled = df.fillna(method='bfill')
            df_ffilled = df.fillna(method='ffill')
            df = pd.concat(df_bfilled, df_ffilled)
            df = df.mean()

        df.loc[:, numeric_cols] = df_numeric
        return df


class MeanFiller(NAFiller):
    def fill_na(self, df):
        numeric_cols = self.get_numeric_columns(df)
        df_numeric = df[numeric_cols]
        df_numeric = df_numeric.fillna(df_numeric.mean())
        df.loc[:, numeric_cols] = df_numeric
        return df


class ZeroFiller(NAFiller):
    def fill_na(self, df):
        numeric_cols = self.get_numeric_columns(df)
        df_numeric = df[numeric_cols]
        df_numeric = df_numeric.fillna(0)
        df.loc[:, numeric_cols] = df_numeric
        return df


class DfFiller:
    def __init__(self, na_fillers_per_cols, default_filler):
        self.na_fillers = na_fillers_per_cols
        self.handled_cols = set(na_fillers_per_cols.columns)
        self.default_filler = default_filler

    def fill_na(self, df):
        for cols, na_filler in self.na_fillers:
            df[cols] = na_filler.fill_na(df[cols])

        unhandled_cols = self.get_unhandled_cols(df)
        df[unhandled_cols] = self.default_filler(unhandled_cols)
        return df

    def get_unhandled_cols(self, df):
        df_numeric_cols = self.get_numeric_columns(df)
        return set(df_numeric_cols) - self.handled_cols

    def get_numeric_columns(self, df):
        return df.select_dtypes(include=np.number).columns.values
