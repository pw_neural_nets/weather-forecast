import pandas as pd
import numpy as np


class TimeSeriesExtractor:
    def __init__(self, time_col, city_col, check_na_cols, allowed_gap_size):
        self.time_col = time_col
        self.city_col = city_col
        self.check_na_cols = check_na_cols
        self.allowed_gap_size = allowed_gap_size

    def get_longest_ts_subseq(self, df):
        df = self.add_missing_dates(df)
        df = self.extract_longest_subseq(df)
        return df

    def extract_longest_subseq(self, df):
        """we know all the dates are present so we can subscribe number to dates"""
        cur_start_idx, cur_sum = df.index.min(), 0
        max_start_idx, max_sum = cur_start_idx, 0
        gap_counter = 0

        for cur_end_idx, row in df.iterrows():
            if not np.isnan(row.WindSpeed) and not np.isnan(row.AirTemp):
                cur_sum += 1
            elif gap_counter > self.allowed_gap_size:
                gap_counter += 1
            else:
                if max_sum < cur_sum:
                    max_start_idx, max_end_idx, max_sum = cur_start_idx, cur_end_idx, cur_sum
                cur_start_idx = cur_end_idx
                gap_counter = 0
                cur_sum = 0
        else:
            if max_sum < cur_sum:
                max_start_idx, max_end_idx, max_sum = cur_start_idx, cur_end_idx, cur_sum

        df = df.loc[max_start_idx:max_end_idx]
        return df

    def add_missing_dates(self, df):
        min_d = df.index.min()
        max_d = df.index.max()
        all_dates = pd.date_range(min_d, max_d)
        df = df.reindex(all_dates).rename_axis(self.time_col)
        return df
