import config
from data_preprocessing.encoding import aggregators
from data_preprocessing.encoding import encoders
from data_preprocessing.encoding import feature_extractors
from data_preprocessing.encoding import na_fillers
from data_preprocessing.encoding import time_series_extractors
from data_preprocessing.encoding import value_mappings

aggregator_mappings = {
    'daily': aggregators.DailyAggregator,
    'multidaily': aggregators.PartOfDayAgg,
}


class EncodingPipeline:
    def pipe_data(self, df):
        return NotImplemented


class DefaultEncodingPipeline(EncodingPipeline):
    def __init__(
            self,
            data_encoder,
            before_agg_feature_expander,
            after_agg_feature_expanders,
            aggregator,
            col_names_to_drop,
    ):
        self.data_encoder = data_encoder
        self.before_agg_feature_expander = before_agg_feature_expander
        self.aggregator = aggregator
        self.after_agg_feature_expanders = after_agg_feature_expanders
        self.col_names_to_drop = col_names_to_drop

    def pipe_data(self, df):
        df = self.data_encoder.encode_columns(df)
        df = self.before_agg_feature_expander.add_new_features(df)
        df = self.aggregator.aggregate(df)
        for expander in self.after_agg_feature_expanders:
            df = expander.add_new_features(df)
        df = df.drop(columns=self.col_names_to_drop)
        return df

    @classmethod
    def build_common(cls, na_filler, na_filler_n_iter, time_col, city_col, check_na_cols, allowed_time_series_gap_size):

        if na_filler is None:
            na_filler = na_fillers.MICEFiller(
                n_iter=na_filler_n_iter,
                time_col=time_col,
                city_col=city_col,
            )

        time_series_extractor = time_series_extractors.TimeSeriesExtractor(
            allowed_gap_size=allowed_time_series_gap_size,
            check_na_cols=check_na_cols,
            time_col=time_col,
            city_col=city_col,
        )

        return na_filler, time_series_extractor

    @classmethod
    def build_default(
        cls,
        day_part_hours,
        na_filler_n_iter,
        col_names_to_drop,
        allowed_time_series_gap_size,
        check_na_cols,
        month_enc,
        aggregator_name,
        wind_dir_col=config.wind_dir_col,
        wind_speed_col=config.wind_speed_col,
        time_col=config.time_col,
        city_col=config.city_col,
        part_of_day_col=config.part_of_day_col,
        cloud_col=config.cloud_col,
        vis_col=config.vis_col,
        ns_wind_col=config.ns_wind_col,
        we_wind_col=config.we_wind_col,
        sin_month_col=config.sin_month_col,
        cos_month_col=config.cos_month_col,
        na_filler=None,
    ):

        data_encoder = encoders.DataEncoder(
            numerical_column_encoders={
                cloud_col: encoders.CloudColEncoder(value_mappings.cloudiness_to_values),
                wind_dir_col: encoders.WindDirColEncoder(value_mappings.wind_dir_to_values),
                vis_col: encoders.VisibilityColEncoder(value_mappings.horizontal_vis_to_values),
            })

        before_agg_feature_expander = feature_extractors.FeaturesExpander(
            feature_extractors={
                (part_of_day_col, time_col): feature_extractors.PartOfDayExtractor(day_part_hours=day_part_hours),
            }
        )

        extractors = {
            (ns_wind_col, wind_dir_col): feature_extractors.CycleExtractor(enc_type="sin", max_val=360, min_val=0),
            (we_wind_col, wind_dir_col): feature_extractors.CycleExtractor(enc_type="cos", max_val=360, min_val=0),
        }

        if month_enc:
            extractors = {
                **extractors,
                **{(sin_month_col, time_col): feature_extractors.MonthExtractor(enc_type="sin", max_val=12, min_val=1),
                   (cos_month_col, time_col): feature_extractors.MonthExtractor(enc_type="cos", max_val=12, min_val=1),
                   }
            }

        after_agg_feature_expanders = [feature_extractors.FeaturesExpander(
            feature_extractors=extractors,
        ), feature_extractors.CityExpander()]

        na_filler, time_series_extractor = cls.build_common(
            na_filler, na_filler_n_iter, time_col, city_col, check_na_cols, allowed_time_series_gap_size)

        aggregator = aggregator_mappings[aggregator_name](
            wind_dir_col=wind_dir_col,
            wind_speed_col=wind_speed_col,
            time_col=time_col,
            city_col=city_col,
            part_of_day_col=part_of_day_col,
            na_filler=na_filler,
            time_series_extractor=time_series_extractor,
        )

        return cls(
            data_encoder=data_encoder,
            before_agg_feature_expander=before_agg_feature_expander,
            after_agg_feature_expanders=after_agg_feature_expanders,
            aggregator=aggregator,
            col_names_to_drop=col_names_to_drop,
        )