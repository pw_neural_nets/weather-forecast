import numpy as np


class NumericalColumnEncoder:
    """Converts columns with descriptive values to numerical"""
    def __init__(self, values_mapping):
        self.values_mapping = values_mapping

    def encode(self, series):
        return series.apply(self.to_numerical)

    def to_numerical(self, row):
        return NotImplemented


class CloudColEncoder(NumericalColumnEncoder):
    def to_numerical(self, row):
        return self.values_mapping[row]


class WindDirColEncoder(NumericalColumnEncoder):
    def to_numerical(self, row):
        if row is np.nan:
            return np.nan
        else:
            return self.values_mapping[row]


class VisibilityColEncoder(NumericalColumnEncoder):
    def to_numerical(self, row):
        if row in self.values_mapping.keys():
            return self.values_mapping[row]
        else:
            return float(row)


class DataEncoder:
    def __init__(self, numerical_column_encoders):
        self.numerical_column_encoders = numerical_column_encoders

    def encode_columns(self, df):
        for col_name, encoder in self.numerical_column_encoders.items():
            df[col_name] = encoder.encode(df[col_name])
        return df
