import math

import pandas as pd


class FeatureExtractor:
    """Creates new feature column from existing one"""
    def get_feature(self, df, source_col):
        return NotImplemented


class CycleExtractor(FeatureExtractor):
    def __init__(self, enc_type, min_val, max_val):
        self.enc_type = enc_type
        self.min_val = min_val
        self.max_val = max_val

    def scale_to_degrees(self, s):
        return (s - self.min_val) / (self.max_val - self.min_val) * 360

    def get_feature(self, df, source_col):
        series = df[source_col]
        series = self.scale_to_degrees(series)
        return series.apply(self.get_encoding_func())

    def get_encoding_func(self):
        if self.enc_type == 'sin':
            return lambda x: math.sin(math.radians(x))
        elif self.enc_type == 'cos':
            return lambda x: math.cos(math.radians(x))
        else:
            raise ValueError('Direction not supported')


class MonthExtractor(CycleExtractor):
    def __init__(self, enc_type, min_val, max_val):
        super().__init__(enc_type, min_val, max_val)

    def get_feature(self, df, source_col):
        month_series = df.index.get_level_values(source_col).month
        month_series = self.scale_to_degrees(month_series)
        month_series = list(map(self.get_encoding_func(), month_series))
        return month_series


class PartOfDayExtractor(FeatureExtractor):
    def __init__(self, day_part_hours):
        self.day_part_hours = day_part_hours

    def get_feature(self, df, source_col):
        series = df[source_col]
        series = pd.to_datetime(series).dt.hour
        return series.apply(lambda x: x // self.day_part_hours).astype('category')


class FeaturesExpander:
    def __init__(self, feature_extractors):
        self.feature_extractors = feature_extractors

    def add_new_features(self, df):
        for (col_name, source_col), extractor in self.feature_extractors.items():
            df[col_name] = extractor.get_feature(df, source_col=source_col)
        return df

class CityExpander:
    def add_new_features(self, df):
        df = df.reset_index(level=[0,1])
        for city in df.City.unique():
            df["City " + city] = 0 + (df.City == city)
        return df
