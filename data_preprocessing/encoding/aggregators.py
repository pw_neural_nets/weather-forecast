import numpy as np
import pandas as pd


class Aggregator:
    def __init__(self, wind_dir_col, wind_speed_col):
        self.wind_dir_col = wind_dir_col
        self.wind_speed_col = wind_speed_col

    def get_wind_aggegator(self, df):
        def get_agg_weighted_wind_deg(row):
            """
            Aggregate wind direction for each day.

            Final wind direction of the day is a sum
            of rows for this day were wind direction is
            weighted by wind speed. Final result is then normalized

            invalid wind direction encoded as negative degrees. Filter them
            """
            valid_row = row[row >= 0]
            if valid_row.empty:
                return np.nan

            idx = row.index
            sub_df = df.loc[idx]
            mult = sub_df[sub_df[self.wind_dir_col] >= 0][self.wind_speed_col]
            norm = mult.sum()
            total_deg = sum(row[row >= 0] * mult)

            if norm != 0:
                return total_deg / norm
            else:
                return 0.0
        return get_agg_weighted_wind_deg


class PartOfDayAgg(Aggregator):
    def __init__(
            self,
            wind_dir_col,
            wind_speed_col,
            time_col,
            city_col,
            part_of_day_col,
            na_filler,
            time_series_extractor,
    ):
        super().__init__(wind_dir_col, wind_speed_col)
        self.time_col = time_col
        self.city_col = city_col
        self.part_of_day_col = part_of_day_col
        self.na_filler = na_filler
        self.time_series_extractor = time_series_extractor
        self.first = 1

    def get_numeric_columns(self, df):
        return df.select_dtypes(include=np.number).columns.values

    def get_grouper_col_to_mean(self, df, exclude_cols):
        numeric_cols = self.get_numeric_columns(df)
        return {col_name: np.mean for col_name in numeric_cols if col_name not in exclude_cols}

    def aggregate(self, df):
        df = self.get_part_of_day_aggregations(df)
        return df

    def get_part_of_day_aggregations(self, df):
        df = self.get_daily_index_df(df)
        df = df.groupby(
            [self.city_col, self.time_col, self.part_of_day_col]
                ).agg({
                    self.wind_speed_col: np.max,
                    self.wind_dir_col: self.get_wind_aggegator(df),
                    **self.get_grouper_col_to_mean(
                        df=df,
                        exclude_cols=(self.wind_speed_col, self.wind_dir_col)
                    )
                }
        )

        df = self.get_longest_times_series_subseq(df)
        return df

    def get_daily_index_df(self, df):
        local_time_idxs = df.loc[:, self.time_col].copy()
        dayli_local_time_idxs = pd.to_datetime(local_time_idxs).dt.normalize()
        df.loc[:, self.time_col] = dayli_local_time_idxs
        return df

    def get_valid_indexes(self, df):
        first_idx = df.first_valid_index()
        last_idx = df.last_valid_index()
        return first_idx, last_idx

    def get_longest_times_series_subseq(self, df):
        df_full_dates = pd.DataFrame()
        df = df.reset_index().set_index(self.time_col)

        for city in df[self.city_col].unique():
            df_all_pod = self.get_filled_city_ts(df, city)
            df_full_dates = df_full_dates.append(df_all_pod)

        df_full_dates = self.set_full_index(df_full_dates)
        return df_full_dates

    def get_filled_city_ts(self, df, city):
        df_city = df.loc[df[self.city_col] == city]

        df_all_pod = pd.DataFrame()
        common_min_date = df_city.index.min()
        common_max_date = df_city.index.max()

        for part_of_day in df[self.part_of_day_col].unique():
            df_pod, common_max_date, common_min_date = self.get_filled_pod_ts(
                df_city,
                part_of_day,
                common_max_date,
                common_min_date
            )
            df_all_pod = df_all_pod.append(df_pod)

        df_all_pod = self.get_common_seq_pod(
            df_all_pod=df_all_pod,
            common_max_date=common_max_date,
            common_min_date=common_min_date,
        )
        df_all_pod[self.city_col] = city
        return df_all_pod

    def get_filled_pod_ts(self, df_city, part_of_day, common_max_date, common_min_date):
        df_pod = df_city[df_city[self.part_of_day_col] == part_of_day]
        df_pod = df_pod.drop(columns=[self.city_col, self.part_of_day_col])
        df_pod = self.time_series_extractor.get_longest_ts_subseq(df_pod)
        common_max_date, common_min_date = self.update_idx_boundaries(
            df_pod=df_pod,
            common_max_date=common_max_date,
            common_min_date=common_min_date,
        )

        df_pod[self.part_of_day_col] = part_of_day
        df_pod = self.fill_na(df_pod)
        return df_pod, common_max_date, common_min_date

    def get_common_seq_pod(self, df_all_pod, common_max_date, common_min_date):
        common_min_date = common_min_date.strftime('%Y-%m-%d')
        common_max_date = common_max_date.strftime('%Y-%m-%d')
        df_all_pod = df_all_pod[common_min_date:common_max_date]
        df_all_pod = df_all_pod.reset_index()
        return df_all_pod

    def update_idx_boundaries(self, df_pod, common_max_date, common_min_date):
        first_idx, last_idx = self.get_valid_indexes(df_pod)
        common_min_date = max(common_min_date, first_idx)
        common_max_date = min(common_max_date, last_idx)
        return common_max_date, common_min_date

    def set_full_index(self, df):
        return df.set_index([self.city_col, self.time_col, self.part_of_day_col])

    def fill_na(self, df):
        df = self.na_filler.fill_na(df)
        return df


class DailyAggregator(PartOfDayAgg):
    def aggregate(self, df):
        df = self.get_part_of_day_aggregations(df)
        df = self.get_daily_aggregation(df)
        return df

    def get_daily_aggregation(self, df):
        """
        different final aggregation might be useful like day/night agg
        check correlation between night and day to see if there is some useful info in it
        """

        df = df.reset_index()
        df = df.groupby(
            [self.city_col, self.time_col]
            ).agg({
                    self.wind_speed_col: np.max,
                    self.wind_dir_col: self.get_wind_aggegator(df),
                    **self.get_grouper_col_to_mean(
                        df=df,
                        exclude_cols=(self.wind_speed_col, self.wind_dir_col)
                    )
                }
        )
        df = df.drop(columns=self.part_of_day_col)
        return df

