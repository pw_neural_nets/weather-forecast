import numpy as np


cloudiness_to_values = {
    np.nan: np.nan,
    'no clouds': 0.0,
    '10%  or less, but not 0': 0.05,
    '20\x9630%.': 0.25,
    '40%.': 0.4,
    '50%.': 0.5,
    '60%.': 0.6,
    '70 \x96 80%.': 0.75,
    '90  or more, but not 100%': 0.95,
    '100%.': 1.0,
    'Sky obscured by fog and/or other meteorological phenomena.': np.nan,
}

wind_dir_to_values = {
    'Wind blowing from the north': 0.0,

    'Wind blowing from the north-west': 315.0,
    'Wind blowing from the north-east': 45.0,
    'Wind blowing from the north-northwest': 338.5,
    'Wind blowing from the north-northeast': 22.5,

    'Wind blowing from the west': 270.0,
    'Wind blowing from the west-northwest': 292.5,
    'Wind blowing from the west-southwest': 247.5,

    'Wind blowing from the east': 90.0,
    'Wind blowing from the east-northeast': 115.5,
    'Wind blowing from the east-southeast': 67.5,

    'Wind blowing from the south': 180.0,
    'Wind blowing from the south-east': 135.0,
    'Wind blowing from the south-west': 225.0,
    'Wind blowing from the south-southwest': 202.5,
    'Wind blowing from the south-southeast': 157.5,
    'variable wind direction': -1,
    'Calm, no wind': -2,
}

horizontal_vis_to_values = {
    'less than 0.05': 0.0,
    'less than 0.1': 0.05,
}
