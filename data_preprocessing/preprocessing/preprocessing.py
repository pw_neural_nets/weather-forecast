import pickle
import functools
import itertools

import numpy as np
from pathlib import Path
from sklearn.preprocessing import MinMaxScaler
from sklearn.decomposition import PCA
import torch

import config


class DataTargetSplitter:
    def __init__(
            self,
            measures_per_day,
            basic_nb_train_dates=config.basic_nb_train_dates,
            basic_target_date=config.basic_target_date,
    ):
        self.measures_per_day = measures_per_day
        self.nb_train_dates = measures_per_day * basic_nb_train_dates
        self.targets_idx = measures_per_day * basic_target_date - measures_per_day

    def get_grouped_samples(self, df):
        """
        Group rows that represent values of measurements for a
        single day/time of the day into a model input
        which is data from X days combined into a single row

        for nb_train_dates = 2 and measures_per_day = 1
               c1           c1_d1 | c1_d2

        day1 |  a      1 |    a      b
        day2 |  b  ->  2 |    b      c
        day3 |  c

        for nb_train_dates = 2 and measures_per_day = 2
                        c1            c1_pod_1_d1 | c1_pod_1_d1 | c1_pod_1_d1 | c1_pod_1_d1

        day1 | pod1 |  a       1   |       a           b             c            d
        day1 | pod2 |  b  ->
        day2 | pod1 |  c
        day2 | pod2 |  d
        """
        city_cols = filter(lambda x: x.startswith("City"), df.columns)
        city_df = df[city_cols]
        df_row = city_df.iloc[0].to_numpy()
        df = df.drop(columns=city_cols)
        data = []
        for idx in range(0, len(df) - self.targets_idx, self.measures_per_day):
            sample = df[idx: idx + self.nb_train_dates].values
            sample = sample.reshape(-1)
            data.append(np.concatenate([sample, df_row]))
        data = np.array(data)
        return data


class MultiDayDataTargetsSplitter(DataTargetSplitter):
    def __init__(
            self,
            target_cols,
            drop_columns,
            measures_per_day,
            basic_nb_train_dates=config.basic_nb_train_dates,
            basic_target_date=config.basic_target_date,
            **kwargs
    ):
        super().__init__(
            measures_per_day=measures_per_day,
            basic_nb_train_dates=basic_nb_train_dates,
            basic_target_date=basic_target_date
        )
        self.target_cols = target_cols
        self.drop_columns = drop_columns

    def split(self, df):
        data = self.extract_per_city(df, self.get_data)
        targets = self.extract_per_city(df, self.get_targets)
        return data, targets

    def extract_per_city(self, df, extract_fn):
        cities = df.City.unique()
        result_per_city = [extract_fn(df[df.City == city]) for city in cities]
        return functools.reduce(self.concat_rows, result_per_city)

    def concat_rows(self, x, y):
        return np.concatenate((x, y), axis=0)

    def get_data(self, df):
        df = df.drop(columns=config.non_numerical_drop_cols)
        df = df.drop(columns=filter(lambda x: x in df.columns, self.drop_columns))
        data = self.get_grouped_samples(df)
        return data

    def get_targets(self, df):
        targets = df[self.target_cols][self.targets_idx:].values
        targets = targets.reshape(-1, self.measures_per_day)
        return targets


class AutoEncoderDataTargetSplitter(MultiDayDataTargetsSplitter):
    """return train data as X and Y"""
    def split(self, df):
        data = self.extract_per_city(df, self.get_data)
        return data, data


class BaselineDataTargetSplitter(MultiDayDataTargetsSplitter):
    """Get only target cols to create predictions as mean or last day"""
    def get_data(self, df):
        df = df[self.target_cols]
        data = self.get_grouped_samples(df)
        return data


class TrainTestSplitter:
    def __init__(self, test_size):
        self.test_size = test_size

    def __call__(self, *arrays):
        if self.test_size == 0:
            return arrays[0], None, arrays[1], None
        else:
            assert len(set(map(len, arrays))) in (0, 1), "Different size of targets and data"
            tuple_arrays = [(array[int(len(array)*self.test_size):, ...], array[:int(len(array)*self.test_size), ...])
                            for array in arrays]
            return tuple(itertools.chain(*tuple_arrays))


class PreprocessingPipeline:
    """df -> data, targets -> data_train, data_test, targets_train, targets_test -> preprocessed..."""
    def __init__(self, preprocessor_data, preprocessor_target, data_targets_splitter, train_test_splitter):
        self.preprocessor_data = preprocessor_data
        self.preprocessor_target = preprocessor_target
        self.data_targets_splitter = data_targets_splitter
        self.train_test_splitter = train_test_splitter

    def preprocess(self, df):
        data, targets = self.data_targets_splitter.split(df)

        data_train, data_test, targets_train, targets_test = self.train_test_splitter(data, targets)

        pre_data_train, pre_data_test = self._preprocess(
            train=data_train,
            test=data_test,
            pre_proc=self.preprocessor_data,
        )

        pre_targets_train, pre_targets_test = self._preprocess(
            train=targets_train,
            test=targets_test,
            pre_proc=self.preprocessor_target
        )

        return pre_data_train, pre_data_test, pre_targets_train, pre_targets_test

    def _preprocess(self, train, test, pre_proc):
        pre_proc.fit(train)
        pre_train = pre_proc.preprocess(array=train)
        pre_test = pre_proc.preprocess(array=test) if test is not None else None
        return pre_train, pre_test

    def get_preprocessors(self):
        preprocessor_data = self.preprocessor_data
        preprocessor_target = self.preprocessor_target
        return {"preprocessor_data": preprocessor_data, "preprocessor_target": preprocessor_target}

    @classmethod
    def build_from_name(cls, name, city, **kwargs):
        test_size = kwargs.pop('test_size')
        enc_name = kwargs.pop('enc_name')
        test_size = test_size if test_size is not None else config.test_size

        if name == 'scaler':
            return cls.build_scaler(test_size, **kwargs)
        elif name == 'pca':
            return cls.build_pca(test_size, **kwargs)
        elif name == 'stationary':
            return cls.build_stationary(test_size, **kwargs)
        elif name == 'baseline':
            return cls.build_baseline(test_size, **kwargs)
        elif name == 'autoencoder':
            return cls.build_autoencoder(test_size, city=city, enc_name=enc_name, **kwargs)
        elif name == 'train_autoencoder':
            return cls.build_train_autoencoder(test_size, **kwargs)
        else:
            raise ValueError('Unsupported preprocessing')

    @classmethod
    def build_scaler(cls, test_size, **kwargs):
        return cls(
            preprocessor_data=ScalerPreprocessor(),
            preprocessor_target=SingleValueScaler(),
            data_targets_splitter=MultiDayDataTargetsSplitter(**kwargs),
            train_test_splitter=TrainTestSplitter(test_size),
        )

    @classmethod
    def build_pca(cls, test_size, **kwargs):
        return cls(
            preprocessor_data=PCAPreprocessor(),
            preprocessor_target=SingleValueScaler(),
            data_targets_splitter=MultiDayDataTargetsSplitter(**kwargs),
            train_test_splitter=TrainTestSplitter(test_size),
        )

    @classmethod
    def build_stationary(cls, test_size, measures_per_day, **kwargs):
        return cls(
            preprocessor_data=StationarityPreprocessor(
                measures_per_day=measures_per_day
            ),
            preprocessor_target=SingleValueScaler(),
            data_targets_splitter=MultiDayDataTargetsSplitter(
                measures_per_day=measures_per_day,
                **kwargs
            ),
            train_test_splitter=TrainTestSplitter(test_size),
        )

    @classmethod
    def build_baseline(cls, test_size, **kwargs):
        return cls(
            preprocessor_data=ScalerPreprocessor(),
            preprocessor_target=SingleValueScaler(),
            data_targets_splitter=BaselineDataTargetSplitter(**kwargs),
            train_test_splitter=TrainTestSplitter(test_size),
        )

    @classmethod
    def build_autoencoder(cls, test_size, city, enc_name, **kwargs):
        return cls(
            preprocessor_data=AutoEncoderPreprocessor(enc_name=enc_name, city=city),
            preprocessor_target=SingleValueScaler(),
            data_targets_splitter=MultiDayDataTargetsSplitter(**kwargs),
            train_test_splitter=TrainTestSplitter(test_size),
        )

    @classmethod
    def build_train_autoencoder(cls, test_size, **kwargs):
        return cls(
            preprocessor_data=ScalerPreprocessor(),
            preprocessor_target=ScalerPreprocessor(),
            data_targets_splitter=AutoEncoderDataTargetSplitter(**kwargs),
            train_test_splitter=TrainTestSplitter(test_size),
        )


class Preprocessor:
    """data -> preprocessed_data"""
    def preprocess(self, array: np.ndarray):
        return NotImplemented

    def inverse_preprocess(self, array: np.ndarray):
        return NotImplemented

    def fit(self, array: np.ndarray):
        return NotImplemented


class SingleValueScaler(Preprocessor):
    """data -> scaled_data"""
    def __init__(self):
        self.max_val = None
        self.min_val = None

    def preprocess(self, array: np.ndarray):
        return (array - self.min_val) / (self.max_val - self.min_val)

    def inverse_preprocess(self, array: np.ndarray):
        return array * (self.max_val - self.min_val) + self.min_val

    def fit(self, array: np.ndarray):
        self.max_val = np.amax(array)
        self.min_val = np.amin(array)


class ScalerPreprocessor(Preprocessor):
    """data -> scaled_data"""
    def __init__(self):
        self.scaler = MinMaxScaler()

    def preprocess(self, array: np.ndarray):
        return self.scaler.transform(array)

    def inverse_preprocess(self, array: np.ndarray):
        return self.scaler.inverse_transform(array)

    def fit(self, array: np.ndarray):
        self.scaler.fit(array)


class PCAPreprocessor(ScalerPreprocessor):
    """
    boilerplate class that needs implementation
    data -> scaled_data -> pca_data"""
    def __init__(self, min_explained_pca=config.min_explained_pca):
        super().__init__()
        self.pca = PCA(min_explained_pca)

    def preprocess(self, array: np.ndarray):
        scaled_array = super().preprocess(array)
        pca_array = self.pca.transform(scaled_array)
        return pca_array

    def inverse_preprocess(self, array: np.ndarray):
        scaled_array = super().inverse_preprocess(array)
        pca_array = self.pca.inverse_transform(scaled_array)
        return pca_array

    def fit(self, array):
        super().fit(array)
        scaled_array = super().preprocess(array)
        self.pca.fit(scaled_array)


class StationarityPreprocessor(ScalerPreprocessor):
    """
    boilerplate class that needs implementation
    data -> scaled_data -> stationarized_data

    example:
        [[1, 2, 3]
         [2, 3, 4]  ->  [ 1,  1,  1]
         [1, 2, 3]]     [-1, -1, -1]
    """
    def __init__(self, measures_per_day, basic_nb_train_dates=config.basic_nb_train_dates):
        super().__init__()
        self.nb_train_dates = measures_per_day * basic_nb_train_dates

    def preprocess(self, array: np.ndarray):
        scaled_array = super().preprocess(array)
        days_array = scaled_array.reshape(len(scaled_array), self.nb_train_dates, -1)
        diff_array = [np.append(arr[0], np.diff(arr, axis=0)) for arr in days_array]
        return diff_array

    def inverse_preprocess(self, array: np.ndarray):
        raise AssertionError(f'{self.__class__.__name__} does not support inverse preprocessing')


class AutoEncoderPreprocessor(ScalerPreprocessor):
    def __init__(self, enc_name, city):
        super().__init__()
        model_file = Path(config.autoencoder_file_location.format(city=city, enc_name=enc_name))
        assert model_file.exists(), f'File "{model_file}" with trained autoencoder does not exist.\n' \
                                    f'Please train the autoencoder or move the file to the correct place'
        with open(model_file, 'rb') as f:
            self.encoder = pickle.load(f).encoder_layers
        self.compressed_size = self.encoder.dense_layers[-1].dense.out_features

    def preprocess(self, array: np.ndarray):
        scaled_array = super().preprocess(array)
        tensor = torch.from_numpy(scaled_array).float()
        out = self.encoder.forward(tensor)
        return out.detach().numpy()

    def inverse_preprocess(self, array: np.ndarray):
        raise AssertionError(f'{self.__class__.__name__} does not support inverse preprocessing')
