import pandas as pd
from pathlib import Path

FILES_IDS = 2

data_folder = Path('./data/projekt_3_dane')
test_path = data_folder/f'test_{id}'
valid_path = data_folder/f'valid_{id}.csv'
target_path = data_folder/f'target_{id}.csv'

for file_id in range(FILES_IDS):

    df_chunk_test = pd.DataFrame()
    df_chunk_valid = pd.DataFrame()

    for city in df_test.City.unique():
        df_city = df_test.loc[df_test.City == city]
        date_idxs = sorted(df_city['Local time'].unique())
        mid_date = date_idxs[len(date_idxs)//2]

        df_city = df_city.sort_values(by='Local time').set_index(['Local time'])

        df_chunk_test = pd.concat([df_chunk_test, df_city[mid_date:]])
        df_chunk_valid = pd.concat([df_chunk_valid, df_city[:mid_date]])

    df_test = pd.read_csv(test_path, sep=';')

    df_chunk_test.to_csv(target_path)
    df_chunk_valid.to_csv(valid_path)
