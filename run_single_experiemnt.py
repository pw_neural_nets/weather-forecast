import random
import torch
import numpy as np

SEED = 3
np.random.seed(SEED)
random.seed(SEED)
torch.manual_seed(SEED)
torch.backends.cudnn.deterministic = True
torch.backends.cudnn.benchmark = False

from experiments import experiments
from experiments.evaluation import evaluation
from experiments.visualization import visualization

epochs = 50
constant_experiment_parameters = {
    'lr': 0.1,
    'momentum': 0.9,
    'bs': 10,
    'epochs': epochs,
    'scheduler_name': 'constant',
}

enc_parameters = {
    'enc_name': 'tridaily_month', 'encoded_df_path': './data/preprocessed/train_1_tridaily_feature_month.csv'}

model_parameters = {'model_name': 'shared_feature_reg', 'target_cols': ['AirTemp']}

experiment_params = experiments.ExperimentParameters(
    preprocessing_name='scaler',
    city='Buenos Aires',
    seed=SEED,
    drop_columns=[],
    **enc_parameters,
    **model_parameters,
    **constant_experiment_parameters,
)

print('exp from paramters')

experiment = experiments.Experiment.from_parameters(
    **experiment_params.return_parameters(
    ignore_parameters=['pos_cls_sampling_mult']),
)

experiment_params.pos_cls_sampling_mult = experiment.get_pos_class_weight()


print('run exp')
exp_result = experiment.run_experiment()

exp_rezs = experiments.ExperimentResults.from_exp(
    exp_params=experiment_params,
    results=exp_result,
    model=experiment.model,
    preprocessor_data=experiment.databunch.train.dataset.preprocessor_data,
    preprocessor_target=experiment.databunch.train.dataset.preprocessor_target,
)

print('run eval')
eval_rez = evaluation.create_evaluation(exp_rezs)['eval']

exp_result.metrics['eval'] = eval_rez

exp_params = exp_rezs.exp_params.return_parameters(ignore_parameters=[])
for k, v in exp_params.items():
    print(k, ': ', v)

print(experiment.model)

print(exp_result)

model = experiment.model
dataloader = experiment.databunch['unshuffled_train']
visualization.plot_preds(dataloader, model, title='Visualization of model predictions on training data')

dataloader = experiment.databunch['test']
visualization.plot_preds(dataloader, model, title='Visualization of model predictions on test data')
