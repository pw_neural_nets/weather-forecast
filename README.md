# Weather Forecast

This repository contains a small framework for short term weather prediction.
Currently, there are [models](../-/wikis/model-architectures) available for temperature regression and wind strength classification.
For more details about the [dataset](../-/wikis/columns description) or used [encodings](../-/wikis/data-encoding) click hyperlinks in the text.
Since there is no relevant literature on the dataset, [baselines](../-/wikis/benchmarks) were 
created and used as a reference point to evaluate the performance of developed models.